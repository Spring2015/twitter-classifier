package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.Saver;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.Utility;
import edu.uic.cs.dm.TwitterClassifier.utility.RunTimer;

/**
 * A base for all the n-gram, words, and POS models.
 * 
 * @author Cosmin Stroe (cstroe@gmail.com)
 *
 */
public abstract class BagOfWordsModel extends AbstractTrainingSetCreator {

	private static Logger LOG = LogManager.getLogger(BagOfWordsModel.class);
	
	protected TweetTokenizer tokenizer;
	
	protected List<String[]> tokenizedTweets;
	
	protected String outputFile;
	
	protected ArffSaver arffSaver;
	
	protected OutputStream os;
	
	/**
	 * @param tokenizedTweetsFile
	 *            The file from which to load the previously tokenized tweets.
	 *            Can be null, in which case the tweets will be tokenized.
	 * @param outputFile
	 *            The file to which the output instances will be saved.
	 */
	public BagOfWordsModel(String tokenizedTweetsFile, String outputFile) {
		super();
		
		this.outputFile = outputFile;
		
		if( tokenizedTweetsFile != null ) {
			// load the tweets.
			tokenizedTweets = loadTokenizedTweets(tokenizedTweetsFile);
		}
		else {
			tokenizer = new PythonTweetTokenizer();
			tokenizer.setCaseSensitive(false);
			tokenizer.setNormalizeUnicode(true);
			tokenizer.setKeepHashTags(true);
		}
		
		arffSaver = new ArffSaver();
		arffSaver.setRetrieval(Saver.INCREMENTAL);
		try {
			this.os = 
					Utility.writeCompressedFile(new File(outputFile), null);
			arffSaver.setDestination(os);
		}
		catch( IOException io ) {
			io.printStackTrace();
			arffSaver = null;
		}
	}
	
	/**
	 * This method should be called once the instances dataset is ready.
	 */
	protected void setArffStructure(Instances instances) {
		arffSaver.setStructure(instances);
	}
	
	protected void writeInstance(Instance i) {
		try {
			arffSaver.writeIncremental(i);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void closeWriter() {
		try {
			os.flush();
			arffSaver.getWriter().flush();
			arffSaver.getWriter().close();
			os.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Use the tweet tokenizer to tokenize the tweets.
	 * 
	 * @param dataFile
	 * @return
	 */
	protected List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile) {
		List<String[]> tokenizedTweets = new ArrayList<String[]>(dataFile.size());
		
		int i = 1;
		final int size = dataFile.size();
		for( AnnotatedTweet tweet : dataFile ) {
			if( (i++ + 1) % 100 == 0 )
				LOG.debug("Tokenizing tweet {}/{}.", i, size);
			final String[] tokenizedTweet = tokenizer.tokenizeTweet(tweet.tweetText);
			if( tokenizedTweet != null ) // skip tokenization errors.
				tokenizedTweets.add( tokenizedTweet );
		}
		return tokenizedTweets;
	}
	
	public void saveTokenizedTweets(String datasetsDirectory, String filename) {
		// Step 1. Tokenize the tweets.
		RunTimer timer = new RunTimer().start();
		List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
		timer.stop();
		LOG.debug("Tokenized {} tweets in {}.", dataFile.size(), timer.getFormattedRunTime());
		
		try {
			OutputStream os = Utility.writeCompressedFile(
					new File(datasetsDirectory + File.separator + filename), null);
			
			ObjectOutputStream out = new ObjectOutputStream(os);
			out.writeObject(tokenizedTweets);
			out.close();
			os.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> loadTokenizedTweets(String tokenizedTweetsFile) {
		// Step 1. Tokenize the tweets.
		List<String[]> tokenizedTweets;
		LOG.debug("Loading tokenized tweets in {}.", tokenizedTweetsFile);
		
		try {
			InputStream is = Utility.openCompressedFile(
					new File(tokenizedTweetsFile), null);
			
			ObjectInputStream in = new ObjectInputStream(is);
			tokenizedTweets = (List<String[]>) in.readObject();
			in.close();
			is.close();
		} catch (IOException i) {
			i.printStackTrace();
			return null;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		return tokenizedTweets;
	}
}
