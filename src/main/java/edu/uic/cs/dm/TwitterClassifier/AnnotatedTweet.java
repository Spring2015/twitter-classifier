package edu.uic.cs.dm.TwitterClassifier;

import java.util.BitSet;

/**
 * This class represents a tweet.
 */
public class AnnotatedTweet {

	final public static int IRRELEVANT = 0;
	final public static int COMPARISON = IRRELEVANT + 1;
	final public static int DONTKNOW = COMPARISON + 1;
	final public static int SARCASTIC = DONTKNOW + 1;
	
	final public int annotationStudent1;
	final public int annotationStudent2;
	final public int annotationFinal;
	
	final public String tweetText;
	final public String tweetDate;
	final public String tweetTime;
	
	final public BitSet tweetRemarks;
	
	public AnnotatedTweet(String tweetDate, String tweetTime, String tweetText,
			int annotationStudent1, int annotationStudent2, int annotationFinal, BitSet tweetRemarks) {
		this.tweetDate = tweetDate;
		this.tweetTime = tweetTime;
		this.tweetText = tweetText;
		this.annotationStudent1 = annotationStudent1;
		this.annotationStudent2 = annotationStudent2;
		this.annotationFinal = annotationFinal;
		this.tweetRemarks = tweetRemarks;
	}
	
	public boolean isIrrelevant() { return tweetRemarks.get(IRRELEVANT); }
	public boolean isComparison() { return tweetRemarks.get(COMPARISON); }
	public boolean isDontKnow()   { return tweetRemarks.get(DONTKNOW); }
	public boolean isSarcastic()  { return tweetRemarks.get(SARCASTIC); }

	@Override
	public String toString() {
		return  tweetText ;
	}
	
}
