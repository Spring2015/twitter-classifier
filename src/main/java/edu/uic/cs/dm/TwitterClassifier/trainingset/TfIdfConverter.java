package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;
import edu.uic.cs.dm.TwitterClassifier.utility.RunTimer;

public class TfIdfConverter {
	private static Logger LOG = LogManager.getLogger(TfIdfConverter.class);

	private void convert(String absolutePath) {
		
		RunTimer timer = new RunTimer().start();
		
		try {
			LOG.debug("Loading the ARFF file.");
			final File inputFile1 = new File(absolutePath);
			BufferedReader reader1 = new BufferedReader(new FileReader(inputFile1));
			ArffReader arff1 = new ArffReader(reader1);
			Instances oldData = arff1.getData();
			oldData.setClassIndex(oldData.numAttributes() - 1);


			// create the attributes for the Instances datastructure
			FastVector attrInfo = new FastVector(oldData.numAttributes() - 1);

			for( int i = 0; i < oldData.numAttributes() - 1; i++ ) {
				Attribute newAttr = new Attribute(oldData.attribute(i).name());
				attrInfo.addElement(newAttr);
			}

			FastVector nominalClassValues = new FastVector();
			nominalClassValues.addElement("negative");
			nominalClassValues.addElement("neutral");
			nominalClassValues.addElement("positive");
			nominalClassValues.addElement("mixed");


			Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
			attrInfo.addElement(finalClassLabel);

			// name of the training set:
			String trainingSetName = "tfidf-" + oldData.relationName();
			Instances tfidfData = new Instances(trainingSetName, attrInfo, oldData.numInstances());


			LOG.debug("Populating the dataset.");

			final int N = oldData.numInstances();

			// compute the ni array
			double[] niArray = new double[oldData.numAttributes() - 1];
			for( int i = 0; i < oldData.numAttributes() - 1; i++ ) { // for every attribute
				double colSum = 0d;
				for( int j = 0; j < N; j++ ) { // sum through all the instances
					colSum += oldData.instance(j).value(i);
				}
				niArray[i] = colSum;
			}
			
			
			for( int i = 0; i < N; i++ ) {
				LOG.debug("Instance:\t{}/{}", i, N);
				
				Instance oldInstance = oldData.instance(i);

				Instance tfidfInstance = new Instance(tfidfData.numAttributes());
				tfidfInstance.setDataset(tfidfData);
				
				for( int j = 0; j < tfidfData.numAttributes() - 1; j++ ) {
					
					// frequency of the current term in the tweet
					double value = oldInstance.value(j);
					
					// maximum frequency for any term in this tweet
					double maxValue = 0d;
					for( int k = 0; k < oldData.numAttributes() - 1; k++ ) {
						if( maxValue < oldInstance.value(k) ) maxValue = oldInstance.value(k);
					}
					
					// tfidf weighted value
					if( maxValue == 0d || niArray[j] == 0d) {
						tfidfInstance.setValue(j, 0d);
					} 
					else {
						double newValue = (value / maxValue) * Math.log( (double) N / niArray[j] );
						tfidfInstance.setValue(j, newValue);
					}
					
				}
				
				// set the class label
				tfidfInstance.setValue(oldData.numAttributes() - 1, oldInstance.stringValue(oldData.numAttributes() - 1));

				tfidfData.add(tfidfInstance);
			}

			final File file1 = new File(absolutePath);
			String outputFile = file1.getParent() + File.separator + tfidfData.relationName() + ".arff";
			LOG.debug("Saving new dataset: {}", outputFile);

			ArffSaver saver = new ArffSaver();
			saver.setInstances(tfidfData);
			saver.setFile(new File(outputFile));
			saver.writeBatch();


		}
		catch (IOException e) {
			LOG.error(e);
		}
		
		timer.stop();
		LOG.debug("Runtime {}.", timer.getFormattedRunTime());
	}

	public static void main(String[] args) {
		Preferences prefs = Preferences.userNodeForPackage(TfIdfConverter.class);

		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile1.getAbsolutePath());

			TfIdfConverter tfIdfConverter = new TfIdfConverter();
			tfIdfConverter.convert(selectedFile1.getAbsolutePath());

		}

	}

}
