package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import cmu.arktweetnlp.Tagger;
import cmu.arktweetnlp.Tagger.TaggedToken;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.TweeterTagger.TwitterPOS;

public class TweeterTaggerModel extends AbstractTrainingSetCreator {

	/**
	 * http://www.ark.cs.cmu.edu/TweetNLP/
	 * https://github.com/brendano/ark-tweet-
	 * nlp/blob/master/docs/annot_guidelines.md
	 */

	private static Logger LOG = LogManager.getLogger(TweeterTaggerModel.class);
	Map<String, Set<TwitterPOS>> vocabPOSTag = new HashMap<String, Set<TwitterPOS>>();
	public static final String MODEL_FILE_NAME = "/tokenizer/model.20120919";

	public void createInstances() {
		// tokenize the tweets
		List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
		// checkPOSTags();
		int counter = 0;
		for (String[] tokenizedTweet : tokenizedTweets) {
			LOG.debug("Tweet number: " + counter++);

			for (int i = 0; i < tokenizedTweet.length; i++) {
				LOG.debug("Tokenized Tweet " + i + " :" + tokenizedTweet[i]);

			}

		}

		// create the vocabulary
		Set<String> vocabulary = buildVocabulary(vocabPOSTag);
		// convert Set to an array of features.
		List<String> vocabularyList = new ArrayList<String>(vocabulary);
		Collections.sort(vocabularyList);
		String[] features = vocabularyList.toArray(new String[0]);

		LOG.debug("Found {} features.", features.length);

		// // create a hashmap for quick checking of string equality of features
		Map<String, Integer> featureIndexMap = new HashMap<String, Integer>();
		for (int i = 0; i < features.length; i++) {
			LOG.debug("vocab : " + i + " " + features[i]);
			featureIndexMap.put(features[i], new Integer(i));
		}

		FastVector attrInfo = new FastVector(features.length);
		for (String currentFeature : features) {
			Attribute currentAttribute = new Attribute(currentFeature);
			attrInfo.addElement(currentAttribute);
		}

		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");

		Attribute finalClassLabel = new Attribute("Final Class Label",
				nominalClassValues);
		attrInfo.addElement(finalClassLabel);

		instances = new Instances("twitter-bowc-pos", attrInfo, dataFile.size());

		for (int c = 0; c < tokenizedTweets.size(); c++) {
			String[] currentTweet = tokenizedTweets.get(c);

			LOG.debug("Current tweet:\t" + c + "/" + tokenizedTweets.size());

			Instance currentDataPoint = new Instance(features.length + 1); // +1
																			// because
																			// of
																			// the
																			// class
																			// label
			currentDataPoint.setDataset(instances);

			for (int i = 0; i < features.length; i++) {
				currentDataPoint.setValue(i, 0.0d);
			}

			Map<String, Integer> tokenCountMap = new HashMap<String, Integer>();
			for (String currentToken : currentTweet) {
				if (tokenCountMap.containsKey(currentToken)) {
					Integer incrementedCount = tokenCountMap.get(currentToken) + 1;
					tokenCountMap.put(currentToken, incrementedCount);
				} else {
					tokenCountMap.put(currentToken, new Integer(1));
				}
			}

			for (String currentToken : tokenCountMap.keySet()) {
				if (featureIndexMap.containsKey(currentToken)) {
					Integer index = featureIndexMap.get(currentToken);
					currentDataPoint
							.setValue(index.intValue(), (double) tokenCountMap
									.get(currentToken).intValue());
				}
			}

			AnnotatedTweet aTweet = dataFile.get(c);

			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the
																				// class
																				// labels
																				// (we
																				// need
																				// this
																				// to
																				// make
																				// naive
																				// bayes
																				// work)
			currentDataPoint.setValue(features.length,
					classes[aTweet.annotationFinal + 1]); // the last feature is
															// the class label

			instances.add(currentDataPoint);
		}

	}

	private Set<String> buildVocabulary(
			Map<String, Set<TwitterPOS>> vocabPOSTag2) {

		HashSet<String> v = new HashSet<String>();

		for (Map.Entry<String, Set<TwitterPOS>> entry : vocabPOSTag.entrySet()) {
			String tweetToken = entry.getKey();
			Set<TwitterPOS> possibleTweetPosTags = entry.getValue();
			 LOG.debug(tweetToken + " tag size : " +
			 possibleTweetPosTags.size()
			 + "  " + possibleTweetPosTags);
			for (TwitterPOS twitterPOS : possibleTweetPosTags) {
				v.add(tweetToken + twitterPOS);

			}
		}

		return v;
	}

	public List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile) {
		return tokenizeTweets(dataFile, false);
	}

	private List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile,
			boolean returnTokensOnly) {

		List<String[]> tokenized = new LinkedList<String[]>();

		for (int dataFileIndex = 0; dataFileIndex < dataFile.size(); dataFileIndex++) {
			LOG.debug("Tokenizing:\t{}/{}", dataFileIndex, dataFile.size());

			AnnotatedTweet tweet = dataFile.get(dataFileIndex);
			String cleanTweet = removeTweetAnnotations(tweet.tweetText);

			SubString[] hashTags = extractSubString(cleanTweet, hashTagPattern);
			cleanTweet = clearSubStrings(cleanTweet, hashTags);

			SubString[] urls = extractSubString(cleanTweet, urlPattern);
			cleanTweet = clearSubStrings(cleanTweet, urls);

			SubString[] ats = extractSubString(cleanTweet, atPattern);
			cleanTweet = clearSubStrings(cleanTweet, ats);

			SubString[] reTweet = extractSubString(cleanTweet, reTweetPattern);
			cleanTweet = clearSubStrings(cleanTweet, reTweet);

			List<String> tweetTokenizedWithPOSTag = new ArrayList<String>();

			List<TaggedToken> taggedTokens = doTokenize(cleanTweet);

			for (TaggedToken token : taggedTokens) {
				if (returnTokensOnly) {
					// do not add POS tag names to tokens
					tweetTokenizedWithPOSTag.add(token.token);
				} else {
					boolean combineFlag = combineVocabAndPOSTags(token.token,
							token.tag);
					if (combineFlag) {
						tweetTokenizedWithPOSTag.add(token.token + token.tag);
					}
				}
			}

			tokenized.add(tweetTokenizedWithPOSTag
					.toArray(new String[tweetTokenizedWithPOSTag.size()]));
		}

		return tokenized;
	}

	private boolean combineVocabAndPOSTags(String tokenTweet, String tag) {
		TwitterPOS twitterPOS = TwitterPOS.get(tag);
		if (twitterPOS == null) {
			LOG.debug("This tag is not in POS tag list : " + tag);
			return false;
		}
		
		
		
		Set<TwitterPOS> tokenTweetPOSSet = vocabPOSTag.get(tokenTweet);
		if (tokenTweetPOSSet == null) {
			tokenTweetPOSSet = new HashSet<TwitterPOS>();
			tokenTweetPOSSet.add(TwitterPOS.get(tag));
			vocabPOSTag.put(tokenTweet, tokenTweetPOSSet);

		} else {
			tokenTweetPOSSet.add(TwitterPOS.get(tag));
			vocabPOSTag.put(tokenTweet, tokenTweetPOSSet);

		}
		return true;
	}

	private List<TaggedToken> doTokenize(String cleanTweet) {
		String modelFilename = MODEL_FILE_NAME;

		Tagger tagger = new Tagger();
		try {
			tagger.loadModel(modelFilename);
		} catch (IOException e) {

			LOG.error(e);
		}

		LOG.debug("clean tweet: " + cleanTweet);
		List<TaggedToken> taggedTokens = null;
		try {
			taggedTokens = tagger.tokenizeAndTag(cleanTweet);
		} catch (Exception e) {
			LOG.error(e);
			taggedTokens = new LinkedList<TaggedToken>();
		}

		return taggedTokens;
	}

	public static void main(String[] args) throws FileNotFoundException {
		// String outputFile =
		// "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-simple.arff";
		String outputFile = "/home/iman/Dropbox/Web Mining/Final Project/testing/testing-romney-pos.arff";

		/*InputStream inputFile = ClassLoader.class
				.getResourceAsStream("/data/training-Obama-tweets.csv");*/
		InputStream inputFile = new FileInputStream(new File("/home/iman/Dropbox/Web Mining/Final Project/testing/testing-Romney-tweets.csv"));

		TweeterTaggerModel creator = null;
		try {
			creator = new TweeterTaggerModel();
		} catch (Exception e) {

			LOG.error(e);
		}
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();
		creator.saveArff(outputFile);

	}

}
