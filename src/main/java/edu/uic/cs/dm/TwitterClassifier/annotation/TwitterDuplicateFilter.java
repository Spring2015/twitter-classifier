package edu.uic.cs.dm.TwitterClassifier.annotation;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;

/**
 * This class contains several pre-processing methods for the tweet dataset.
 * 
 * <ol>
 * <li>Duplicate tweet aggregation.</li>
 * </ol>
 * 
 * @author cosmin
 *
 */
public class TwitterDuplicateFilter {

	private static String inputFilePath = "/home/cosmin/Desktop/TwitterData/during.2.part";
	private static String outputFilePath = "/home/cosmin/Desktop/TwitterData/during.2.part.sorted";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader bfr = new BufferedReader(new FileReader(new File(inputFilePath)));
		
		String currentLine = null;
		int lineCount = 0;
		MultiMap tweetMap = new MultiValueMap();
		while( (currentLine = bfr.readLine()) != null ) {
			// strip urls
			String[] splitLine = currentLine.split("\t");
			if( splitLine.length != 2 ) {
				System.err.println("Error with line: " + currentLine);
				continue;
			}
			
			String cleanedTweet = currentLine.split("\t")[1]
					.replaceAll("http://[a-z]+\\.[a-z]+/[a-zA-Z0-9]{0,8}", "") // remove URLs
					.replaceAll("@[a-zA-Z0-9]+", "")  // remove @s
					.replaceAll("#[a-zA-Z0-9]+", "")  // remove hash tags
					.replaceAll("\\s+", " ")          // normalize spaces
					.trim();
			
			tweetMap.put(cleanedTweet, currentLine);
			lineCount++;
		}
		
		bfr.close();
		
		System.out.println("Original tweets: " + lineCount);
		System.out.println("Unique tweets: " + tweetMap.keySet().size());

		
		BufferedWriter bfw = new BufferedWriter(new FileWriter(new File(outputFilePath)));
		
		for( Object key : tweetMap.keySet() ) {
			@SuppressWarnings("unchecked")
			Collection<String> values = (Collection<String>) tweetMap.get(key);
			for( String currentValue : values ) {
				bfw.write(currentValue);
				bfw.write('\n');
			}
			
		}
	}
	
}
