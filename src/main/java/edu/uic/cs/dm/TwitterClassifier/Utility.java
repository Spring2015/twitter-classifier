package edu.uic.cs.dm.TwitterClassifier;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

public class Utility {

	public static boolean dirExists(String directory) {
		final File dir = new File(directory);
		if( !dir.isDirectory() ) return false;
		if( !dir.canWrite() ) return false;
		
		return true;
	}
	/**
	 * Open a file that's possibly compressed, with an extra check for an
	 * uncompressed file extension.
	 * 
	 * @param inputFile
	 *            <p>
	 *            This file can be compressed or uncompressed. If it is
	 *            compressed, we will apply the appropriate decompression
	 *            streams.
	 *            </p>
	 * 
	 * @param uncompressedFileExtension
	 *            <p>
	 *            This is the extension of the uncompressed file. Usually
	 *            GZipping or BZipping a file appends a ".gz" or a ".bz2" file
	 *            extension, so we need the extension of the file before it was
	 *            compressed. For example if I have a file "data.xml.bz2" the
	 *            uncompressed extension is ".xml". Similarly, if I have
	 *            "data.bin.gz", the uncompressed file extension is ".bin". If
	 *            you don't need to check for an uncompressed file extension,
	 *            just set this to null.
	 *            </p>
	 * 
	 * @return An input stream to the uncompressed content of the file.
	 * @throws FileNotFoundException
	 *             If the uncompressed file extension does not match the file.
	 */
	public static InputStream openCompressedFile(File inputFile, String uncompressedFileExtension) 
			throws FileNotFoundException, IOException {
		
		if( uncompressedFileExtension == null ) uncompressedFileExtension = "";
		
		if( inputFile.getName().toLowerCase().endsWith(uncompressedFileExtension + ".bz2") ) {
			// we are reading a compressed XML file.
			final InputStream fin = new FileInputStream(inputFile);
			final InputStream bzin = new BZip2CompressorInputStream(fin);
			return new BufferedInputStream(bzin);
		}
		else if( inputFile.getName().toLowerCase().endsWith(uncompressedFileExtension + ".gz") ) {
			final InputStream fin = new FileInputStream(inputFile);
			final InputStream zin = new GZIPInputStream(fin);
			return new BufferedInputStream(zin);
		}
		else if( uncompressedFileExtension.isEmpty() ) {
			final InputStream fin = new FileInputStream(inputFile);
			return new BufferedInputStream(fin);
		}
		else if( inputFile.getName().toLowerCase().endsWith(uncompressedFileExtension) ) {
			final InputStream fin = new FileInputStream(inputFile);
			return new BufferedInputStream(fin);
		}
		
		throw new FileNotFoundException("The uncompressed file extension '"
				+ uncompressedFileExtension + "' does not match this file '"
				+ inputFile.getName() + "'.");
	}
	
	public static OutputStream writeCompressedFile(File outputFile, String uncompressedFileExtension) 
			throws FileNotFoundException, IOException {
		
		if( uncompressedFileExtension == null ) uncompressedFileExtension = "";
		
		if( outputFile.getName().toLowerCase().endsWith(uncompressedFileExtension + ".bz2") ) {
			// we are reading a compressed XML file.
			final OutputStream fin = new FileOutputStream(outputFile);
			final OutputStream bzin = new BZip2CompressorOutputStream(fin);
			return new BufferedOutputStream(bzin);
		}
		else if( outputFile.getName().toLowerCase().endsWith(uncompressedFileExtension + ".gz") ) {
			final OutputStream fin = new FileOutputStream(outputFile);
			final OutputStream zin = new GZIPOutputStream(fin);
			return new BufferedOutputStream(zin);
		}
		else if( uncompressedFileExtension.isEmpty() ||
				 outputFile.getName().toLowerCase().endsWith(uncompressedFileExtension) ) {
			final OutputStream fin = new FileOutputStream(outputFile);
			return new BufferedOutputStream(fin);
		}
		
		throw new FileNotFoundException("The uncompressed file extension '"
				+ uncompressedFileExtension + "' does not match this file '"
				+ outputFile.getName() + "'.");
	}
}
