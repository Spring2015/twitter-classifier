package edu.uic.cs.dm.TwitterClassifier.trainingset;

/**
 * An abstract interface for a TweetTokenizer.
 *  
 * @author Cosmin Stroe (cstroe@gmail.com)
 *
 */
public interface TweetTokenizer {

	public String[] tokenizeTweet(String rawTweet);
	
	public void setNormalizeUnicode(boolean normalizeUnicode);
	
	public void setCaseSensitive(boolean caseSensitive);
	
	public void setKeepHashTags(boolean keepHashTags);
	
}
