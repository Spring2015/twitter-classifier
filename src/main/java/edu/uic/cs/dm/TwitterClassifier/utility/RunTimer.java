package edu.uic.cs.dm.TwitterClassifier.utility;

/**
 * A simple class that implements a run timer.
 */
public class RunTimer {

	private long startTime;
	private long endTime; 
	private long totTime;
	private boolean stopped = false;
	
	private static final long MILLI = 1000000l;
	
	public RunTimer() { totTime = 0; }
	
	public RunTimer start() { 
		startTime = System.nanoTime() / MILLI;
		stopped = false;
		return this;
	}
	public RunTimer stop() {
		getRunTime();
		stopped = true;
		return this;
	}
	public RunTimer reset() {
		endTime = startTime = totTime = 0;
		stopped = true;
		return this;
	}
	public RunTimer resetAndStart() {
		reset();
		start();
		return this;
	}
	
	/**
	 * @return The elapsed time since the timer was started, in milliseconds.
	 */
	public long getRunTime() {
		if( !stopped ) {
			endTime = System.nanoTime() / MILLI;
			totTime = endTime - startTime;
		}
		return totTime;
	}

	
	
	public String getFormattedRunTime() {
		return RunTimer.getFormattedTime(getRunTime());
	}
	
	public static String getFormattedTime(long totMS){

		//return the time in hh:mm:ss:msmsms starting from the total time in ms

		//msmsms
		long msmsms = totMS % 1000;
		//ss
		long totS = totMS / 1000l;
		long ss = totS % 60l;
		//mm
		long totM = totS / 60l;
		long mm = totM % 60;
		//hh
		long totH = totM / 60l;
		long hh = totH % 60;


		return hh+"h "+mm+"m "+ss+"s "+msmsms+"ms";
	}
}
