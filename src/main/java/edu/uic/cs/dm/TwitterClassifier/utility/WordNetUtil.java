package edu.uic.cs.dm.TwitterClassifier.utility;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class WordNetUtil {
	
	private static Logger LOG = LogManager.getLogger(WordNetUtil.class);
	
	
	private static WordNetDatabase wordNet; 
	
	HashMap<String, Boolean> isSynonym = new HashMap<String, Boolean>();
	
	public WordNetUtil(String wordNetDir){
		initWordnet(wordNetDir);
	}
	
	private void initWordnet(String wordNetDir) {
		if( wordNet != null ) return; // skip the initialization, wordnet has already been initialized
		
		if( wordNetDir == null ) {
			LOG.error("Could not find WordNet directory!");
			return;
		}
		
		System.setProperty("wordnet.database.dir", wordNetDir);
		// Instantiate 
		try {
			wordNet = WordNetDatabase.getFileInstance();
		}
		catch( Exception e ) {
			LOG.error("Cannot open WordNet files.\nWordNet should be in: {}", wordNetDir);
		}
	}

	public boolean areSynonyms(String source, String target) {
	
		String key = source + ":" + target;
		Boolean answer = isSynonym.get(key);
		if(answer != null) return answer;
		
		Synset[] sourceSynsets = wordNet.getSynsets(source);
		Synset[] targetSynsets = wordNet.getSynsets(target);
		
		for (int i = 0; i < sourceSynsets.length; i++) {
			for (int j = 0; j < targetSynsets.length; j++) {
				if(sourceSynsets[i] == targetSynsets[j]){
					//System.out.println(source + " " + target + " synonyms!!");
					isSynonym.put(key, true);
					return true;
				}
			}
		}
		isSynonym.put(key, false);
		return false;
	}
	
	public WordNetDatabase getWordNet() {
		return wordNet;
	}
}
