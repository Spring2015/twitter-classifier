package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.List;

import junit.framework.TestCase;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;

public class TestAbstractTrainingSetCreator extends TestCase {

	private final String sampleCSVFile = 
			"Tween-id,date,time,Anootated tweet,sentiment class label,,,\"1: positive, -1: negative, 0: neutral, 2: mixed\",,,,,student1,student2,final class (consensus),,\n" +
			"1,10/16/12,09:38:08-05:00,Insidious!<e>Mitt Romney</e>'s Bain Helped Philip Morris Get U.S. High Schoolers <a>Hooked On Cigarettes</a> http://t.co/nMKuFcUq via @HuffPostPol,-1,-1,-1,,\n" +
			"2,10/16/12,10:22:34-05:00,Senior <e>Romney</e> Advisor Claims <e>Obama</e> Administration Is Deliberately <a>Misleading Public On Libya</a>: http://t.co/cpJjGsCF tp #US,2,2,2,,\n" +
			"3,10/16/12,10:14:18-05:00,.@WardBrenda @shortwave8669 @allanbourdius you mean like <e>romney </e><a>cheated in primary</a>?,-1,-1,-1,,\n" +
			"4,10/16/12,09:27:16-05:00,<e>Mitt Romney</e> still doesn't <a>believe</a> that we <a>have a black president</a>.,-1,-1,-1,sarchastic,\n" +
			"5,10/16/12,10:11:43-05:00,<e>Romney</e>'s <a>tax plan</a> deserves a 2nd look because he has a secret one that's different than the one <a>he's been lying about</a>http://t.co/arvfPQ7W,-1,-1,-1,,\n" +
			"6,10/16/12,10:13:17-05:00,Hope <e>Romney</e> debate prepped w/ the same people as last time.,1,1,1,,\n" +
			"7,10/16/12,10:17:28-05:00,Want to know how <e>Mitt Romney</e> is going to be able to <a>cut by $5 trillon dollars</a>? Go here it explains everything: http://t.co/t8jYT5RH,-1,-1,-1,,\n" +
			"8,10/16/12,09:35:55-05:00,\"If <e>Romney</e> wins the <a>presidential election</a>, the<a> worlds really ending this year.<a>\",-1,-1,-1,,\n" +
			"9,10/16/12,09:33:07-05:00,\"Presidential debate round 2: <e>Romney</e> wants a repeat, <e>Obama</e> a reversal http://t.co/ode4bA1q\",2,2,2,,\n" +
			"10,10/16/12,09:40:14-05:00,Someone on the <e>mitt Romney</e> <a>Facebook page</a> said president bush kept us safe... Uhh since when,0,-1,-1,irrelevent,";

	
	/**
	 * TODO: Test UNICODE chars, date and time.
	 */
	public void testCSVReader() {
		
		TrainingSetCreator creator = new AbstractTrainingSetCreator() {
			
			public void createInstances() {
			}
		};
		
		creator.setInputSource(new StringReader(sampleCSVFile));
		
		List<AnnotatedTweet> tweets = creator.getDataFile();
		
		assertEquals(10, tweets.size());
		assertEquals(tweets.get(0).annotationStudent1, -1);
		assertEquals(tweets.get(0).annotationStudent2, -1);
		assertEquals(tweets.get(0).annotationFinal, -1);
		assertEquals(tweets.get(0).tweetText, "Insidious!<e>Mitt Romney</e>'s Bain Helped Philip Morris Get U.S. High Schoolers <a>Hooked On Cigarettes</a> http://t.co/nMKuFcUq via @HuffPostPol");
		
		assertEquals(tweets.get(1).annotationStudent1, 2);
		assertEquals(tweets.get(1).annotationStudent2, 2);
		assertEquals(tweets.get(1).annotationFinal, 2);
		
		assertEquals(tweets.get(7).annotationStudent1, -1);
		assertEquals(tweets.get(7).annotationStudent2, -1);
		assertEquals(tweets.get(7).annotationFinal, -1);
		assertEquals(tweets.get(7).tweetText, "If <e>Romney</e> wins the <a>presidential election</a>, the<a> worlds really ending this year.<a>");

		assertEquals(tweets.get(9).annotationStudent1, 0);
		assertEquals(tweets.get(9).annotationStudent2, -1);
		assertEquals(tweets.get(9).annotationFinal, -1);
		
	}
	
	public void testCVSReader() {
		
		TrainingSetCreator creator = new AbstractTrainingSetCreator() {
			
			public void createInstances() {
			}
		};
		
		final InputStream inputFile = ClassLoader.class.getResourceAsStream("/data/training-Obama-tweets-old.csv");
		
		creator.setInputSource(new InputStreamReader(inputFile));
		
		List<AnnotatedTweet> tweets = creator.getDataFile();
		
		assertEquals(7188, tweets.size());
		
	}
}
