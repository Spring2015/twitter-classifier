package edu.uic.cs.dm.TwitterClassifier.trainingset.arff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ArffLoader.ArffReader;

public class ArffAttributeMerger {

	private static Logger LOG = LogManager.getLogger(ArffAttributeMerger.class);
	
	
	private void merge(String absolutePath1, String absolutePath2) {
		
		try {
			LOG.debug("Loading first ARFF file.");
			final File inputFile1 = new File(absolutePath1);
			BufferedReader reader1 = new BufferedReader(new FileReader(inputFile1));
			ArffReader arff1 = new ArffReader(reader1);
			Instances data1 = arff1.getData();
			data1.setClassIndex(data1.numAttributes() - 1);
			
			LOG.debug("Loading second ARFF file.");
			final File inputFile2 = new File(absolutePath2);
			BufferedReader reader2 = new BufferedReader(new FileReader(inputFile2));
			ArffReader arff2 = new ArffReader(reader2);
			Instances data2 = arff2.getData();
			data2.setClassIndex(data2.numAttributes() - 1);
			
			LOG.debug("Creating merged attributes...");
			
			Set<String> mergedAttributeNames = new HashSet<String>();
			
			{
				Enumeration data1Attributes = data1.enumerateAttributes();
				while( data1Attributes.hasMoreElements() ) {
					Attribute currentAttribute = (Attribute) data1Attributes.nextElement();
					if( currentAttribute.name().equals("Final Class Label") ) continue; // skip the class label
					mergedAttributeNames.add(currentAttribute.name());
				}
				
				Enumeration data2Attributes = data2.enumerateAttributes();
				while( data2Attributes.hasMoreElements() ) {
					Attribute currentAttribute = (Attribute) data2Attributes.nextElement();
					if( currentAttribute.name().equals("Final Class Label") ) continue; // skip the class label
					mergedAttributeNames.add(currentAttribute.name());
				}
			}
			
			// create the attributes for the Instances datastructure
			FastVector mergedAttributes = new FastVector(mergedAttributeNames.size());
			
			for( String mergedAttributeName : mergedAttributeNames ) {
				Attribute newAttr = new Attribute(mergedAttributeName);
				mergedAttributes.addElement(newAttr);
			}
			
			FastVector nominalClassValues = new FastVector();
			nominalClassValues.addElement("negative");
			nominalClassValues.addElement("neutral");
			nominalClassValues.addElement("positive");
			nominalClassValues.addElement("mixed");
			
			
			Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
			mergedAttributes.addElement(finalClassLabel);
			
			String trainingSetName = "combined-" + data1.relationName() + "-" + data2.relationName();
			Instances mergedData = new Instances(trainingSetName, mergedAttributes, data1.numInstances());
			
			LOG.debug("Creating maps...");
			
			Map<Integer,Integer> data1toMerged = new HashMap<Integer,Integer>();
			
			for( int i = 0; i < mergedAttributes.size(); i++ ) {
				Attribute mergedAttr = (Attribute) mergedAttributes.elementAt(i);
				Enumeration data1Attributes = data1.enumerateAttributes();
				for( int j = 0; data1Attributes.hasMoreElements(); j++ ) {
					Attribute data1Attribute = (Attribute) data1Attributes.nextElement();
					
					if( mergedAttr.name().equals(data1Attribute.name()) ) {
						data1toMerged.put(new Integer(j), new Integer(i));
					}
				}
			}
			
			Map<Integer,Integer> data2toMerged = new HashMap<Integer,Integer>();
			
			for( int i = 0; i < mergedAttributes.size(); i++ ) {
				Attribute mergedAttr = (Attribute) mergedAttributes.elementAt(i);
				Enumeration data2Attributes = data2.enumerateAttributes();
				for( int j = 0; data2Attributes.hasMoreElements(); j++ ) {
					Attribute data2Attribute = (Attribute) data2Attributes.nextElement();
					
					if( mergedAttr.name().equals(data2Attribute.name()) ) {
						data2toMerged.put(new Integer(j), new Integer(i));
					}
				}
			}
			
			for( int i = 0; i < data1.numInstances(); i++ ) {
				
				LOG.debug("Data 1 Instance {}", i);
				
				Instance newInstance = new Instance(mergedAttributes.size());
				
				// clear the new instance
/*				for( int j = 0; j < newInstance.numAttributes() - 1; j++ ) {
					newInstance.setValue(j, 0d);
				}*/
				
				// fill in the new instance
				Instance currentData1Instance = data1.instance(i);
				for( int j = 0; j < currentData1Instance.numAttributes(); j++ ) {
					Integer mapped = data1toMerged.get(new Integer(j));
					if( mapped == null ) continue;
					newInstance.setValue(mapped.intValue(), currentData1Instance.value(j));
				}
				
				mergedData.add(newInstance);
			}
			
			
			for( int i = 0; i < data2.numInstances(); i++ ) {
				
				LOG.debug("Data 2 Instance {}", i);
				
				Instance newInstance = new Instance(mergedAttributes.size());
				
				// clear the new instance
/*				for( int j = 0; j < newInstance.numAttributes() - 1; j++ ) {
					newInstance.setValue(j, 0d);
				}*/
				
				// fill in the new instance
				Instance currentData2Instance = data2.instance(i);
				for( int j = 0; j < currentData2Instance.numAttributes(); j++ ) {
					Integer mapped = data2toMerged.get(new Integer(j));
					if( mapped == null ) continue;
					newInstance.setValue(mapped.intValue(), currentData2Instance.value(j));
				}
				
				mergedData.add(newInstance);
			}
			
			final File file1 = new File(absolutePath1);
			String outputFile = file1.getParent() + File.separator + mergedData.relationName() + ".arff";
			LOG.debug("Saving new dataset: {}", outputFile);

			ArffSaver saver = new ArffSaver();
			saver.setInstances(mergedData);
			saver.setFile(new File(outputFile));
			saver.writeBatch();
		}
		catch( IOException e ) {
			LOG.error(e);
		}
		
	}
	
	
	public static void main(String[] args) {
		Preferences prefs = Preferences.userNodeForPackage(ArffCombiner.class);

		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile1.getAbsolutePath());
			if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
				File selectedFile2 = fc.getSelectedFile();
				ArffAttributeMerger arffMerger= new ArffAttributeMerger();
				arffMerger.merge(selectedFile1.getAbsolutePath(), selectedFile2.getAbsolutePath());
			}
		}

	}
	
}
