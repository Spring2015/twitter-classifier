package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;

/**
 * Filter features by their support.  This works directly on the ARFF file.
 * 
 * @author cosmin
 *
 */
public class MinSupportFilter {

	private static Logger LOG = LogManager.getLogger(MinSupportFilter.class);
	
	public static void filterByMinSupport(String inputFilePath, int minimumSupportFilter) {
		try {
			LOG.debug("Loading the ARFF file.");
			final File inputFile = new File(inputFilePath);
			BufferedReader reader = new BufferedReader(new FileReader(inputFile));
			ArffReader arff = new ArffReader(reader);
			Instances oldData = arff.getData();
			oldData.setClassIndex(oldData.numAttributes() - 1);
						
			LOG.debug("Computing the feature support.");

			int[] featureSupport = new int[oldData.numAttributes()];

			for( int i = 0; i < oldData.numInstances(); i++ ) {
				Instance currentInstance = oldData.instance(i);
				
				for( int j = 0; j < currentInstance.numAttributes(); j++ ) {
					if( currentInstance.value(j) > 0d )
						featureSupport[j]++;
				}
				featureSupport[currentInstance.numAttributes()-1] = Integer.MAX_VALUE; // this is the sentiment label
			}
			
			LOG.debug("Identifying features that do not meet the minimum support.");
			
			Set<Integer> skipList = new HashSet<Integer>();

			for( int i = 0; i < featureSupport.length; i++ ) {
				if( featureSupport[i] < minimumSupportFilter ) {
					skipList.add(new Integer(i));
				}
			}
			
			LOG.debug("Identified {} features to skip.", skipList.size());
			
			LOG.debug("Creating the new dataset.");
			
			FastVector newAttrInfo = new FastVector();
			
			for( int i = 0; i < oldData.numAttributes(); i++ ) {
				if( skipList.contains(new Integer(i)) ) continue; // do not include this attribute
				Attribute oldAttr = oldData.attribute(i);
				
				// cannot reuse the old attribute, we must create a new one.
				if( oldAttr.isNumeric() ) {
					Attribute newAttr = new Attribute(oldAttr.name());
					newAttrInfo.addElement(newAttr);
				}
				else if( oldAttr.isNominal() ) {
					FastVector values = new FastVector();
					for( int j = 0; j < oldAttr.numValues(); j++ ) {
						values.addElement(oldAttr.value(j));
					}
					
					Attribute newAttr = new Attribute(oldAttr.name(), values);
					newAttrInfo.addElement(newAttr);
				}
				else {
					throw new RuntimeException("Unhandled type of attribute.");
				}
			}
			
			String newRelationName;
			if( oldData.relationName().contains("minsup") ) {
				newRelationName = oldData.relationName().replaceAll("minsup:[0-9]+", "minsup:"+Integer.toString(minimumSupportFilter));
			}
			else {
				newRelationName = oldData.relationName() + "-minsup:" + Integer.toString(minimumSupportFilter);
			}
			
			Instances newData = new Instances( newRelationName, newAttrInfo, oldData.numInstances() );
			
			LOG.debug("Found {} features after filtering.", newData.numAttributes());
			
			LOG.debug("Populating the new dataset.");
			
			for( int i = 0; i < oldData.numInstances(); i++ ) {
				Instance oldInstance = oldData.instance(i);

				LOG.debug("Instance {}/{}. Sentiment: {}", i+1, oldData.numInstances(), oldInstance.stringValue(oldInstance.numAttributes()-1));
								
				Instance newInstance = new Instance(newAttrInfo.size());
				newInstance.setDataset(newData);
				
				for( int j = 0, k = 0; j < oldData.numAttributes(); j++) {
					if( skipList.contains(new Integer(j)) ) continue; // skip the attribute
					
					Attribute currentAttribute = oldData.attribute(j);
					if( currentAttribute.isNumeric() ) {
						newInstance.setValue(k, oldInstance.value(j));
					}
					else {
						final String val = oldInstance.stringValue(j);
						newInstance.setValue(k, val);
					}
					
					k++;
				}
				
				newData.add(newInstance);
				
			}
			
			LOG.debug("Saving new dataset.");
	
			String outputFile = inputFile.getParent() + File.separator + newData.relationName() + ".arff";
			
			ArffSaver saver = new ArffSaver();
			saver.setInstances(newData);
			saver.setFile(new File(outputFile));
			saver.writeBatch();
			
		}
		catch(IOException e) {
			LOG.error(e);
		}
		
	}
	
	public static void main(String[] args) {
		
		Preferences prefs = Preferences.userNodeForPackage(MinSupportFilter.class);
		
		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		
		if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
			File selectedFile = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile.getAbsolutePath());
			
			String input = JOptionPane.showInputDialog("Minimum support?");
			
			if( input == null ) return;
			
			int minSup = 0;
			minSup = Integer.parseInt(input);
			
			filterByMinSupport(selectedFile.getAbsolutePath(), minSup);
			
		}
	}
	
}
