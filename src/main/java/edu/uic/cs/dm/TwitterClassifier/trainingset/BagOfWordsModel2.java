package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.util.Version;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.Utility;

/**
 * An improvement over {@link UnigramModel}:
 * 
 * <ul>
 * <li>Lowercase transformation.</li>
 * <li>Tokenization from Lucene StandardAnalyzer.</li>
 * <li>Stopword removal via StopFilter.</li>
 * </ul>
 * 
 * @author cosmin
 *
 */
public class BagOfWordsModel2 extends AbstractTrainingSetCreator {

	private static Logger LOG = LogManager.getLogger(BagOfWordsModel2.class);
	
	private boolean caseSensitiveTerms = false;
	private boolean caseSensitiveTags = false;
	
	public void createInstances() {
		
		LOG.debug("Tokenizing tweets.");
		List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
		
		LOG.debug("Building the vocabulary.");
		Set<String> vocabulary = buildVocabulary(tokenizedTweets);		
		String[] features = vocabulary.toArray(new String[0]);
		
		LOG.trace("Found {} features.", features.length);
		
		// create a hashmap for quick checking of string equality of features
		Map<String,Integer> featureIndexMap = new HashMap<String,Integer>();
		for(int i = 0; i < features.length; i++) {
			featureIndexMap.put(features[i], new Integer(i));
		}
		
		// create the attributes for the Instances datastructure
		FastVector attrInfo = new FastVector(features.length+1);
		for( String currentFeature : features ) {
			Attribute currentAttribute = new Attribute(currentFeature);
			attrInfo.addElement(currentAttribute);
		}
		
		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");
		
		
		Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
		attrInfo.addElement(finalClassLabel);
		
		// name of the training set:
		String trainingSetName = "bowc2-csterm:" + Boolean.toString(caseSensitiveTerms) + "-cstag:" + Boolean.toString(caseSensitiveTags);
		instances = new Instances(trainingSetName, attrInfo, dataFile.size());
		
		for (int c = 0; c < tokenizedTweets.size(); c++ ) {
			String[] currentTweet = tokenizedTweets.get(c);
			
			LOG.debug("Current tweet:\t" + c + "/" + tokenizedTweets.size());
			
			Instance currentDataPoint = new Instance(features.length+1); // +1 because of the class label
			currentDataPoint.setDataset(instances);
			
			for( int i = 0; i < features.length; i++) {
				currentDataPoint.setValue(i, 0.0d);
			}
			
			Map<String,Integer> tokenCountMap = new HashMap<String,Integer>();
			for(String currentToken : currentTweet) {
				if( tokenCountMap.containsKey(currentToken) ) {
					Integer incrementedCount = tokenCountMap.get(currentToken) + 1;
					tokenCountMap.put(currentToken, incrementedCount);
				}
				else {
					tokenCountMap.put(currentToken, new Integer(1));
				}
			}
			
			for(String currentToken : tokenCountMap.keySet()) {
				if( featureIndexMap.containsKey(currentToken) ) {
					Integer index = featureIndexMap.get(currentToken);
					currentDataPoint.setValue(index.intValue(), (double)tokenCountMap.get(currentToken).intValue() );
				}
			}
						
			AnnotatedTweet aTweet = dataFile.get(c);
			
			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the class labels ( we need this to make naive bayes work )
			
			currentDataPoint.setValue(features.length, classes[aTweet.annotationFinal+1]);  // the last feature is the class label
			
			instances.add(currentDataPoint);
		}
	}

	/**
	 * @return A list containing the tokens for each tweet.
	 */
	private List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile) {
				
		List<String[]> tokenized = new LinkedList<String[]>();
		
		for(AnnotatedTweet tweet : dataFile) {
			String cleanTweet = removeTweetAnnotations(tweet.tweetText);
			
			// remove hashtags
			SubString[] hashTags = extractSubString(cleanTweet, hashTagPattern);
			cleanTweet = clearSubStrings(cleanTweet, hashTags);

			// remove urls
			SubString[] urls = extractSubString(cleanTweet, urlPattern);
			cleanTweet = clearSubStrings(cleanTweet, urls);
			
			try {
				List<String> tweetTokens = removeStopWordsAndStem(cleanTweet);

				// add hashtags to the end of the tokens.
				for( SubString htag : hashTags ) {
					if( !caseSensitiveTags ) {
						tweetTokens.add(htag.text.toLowerCase());
					}
					else {
						tweetTokens.add(htag.text);
					}
				}
				
				tokenized.add(tweetTokens.toArray(new String[0]));
			}
			catch(IOException e) {
				LOG.error(e);
			}
		}
		
		return tokenized;
	}
	
	public List<String> removeStopWordsAndStem(String input) throws IOException {

	    TokenStream tokenStream = new StandardTokenizer(
	            Version.LUCENE_40, new StringReader(input.toLowerCase()));
	    tokenStream = new StopFilter(Version.LUCENE_40, tokenStream, StopAnalyzer.ENGLISH_STOP_WORDS_SET);
	    //tokenStream = new PorterStemFilter(tokenStream);

	    List<String> termList = new LinkedList<String>();
	    CharTermAttribute termAttr = tokenStream.getAttribute(CharTermAttribute.class);
	    while (tokenStream.incrementToken()) {
	    	if( !caseSensitiveTerms ) {
	    		termList.add(termAttr.toString().toLowerCase());
	    	}
	    	else {
	    		termList.add(termAttr.toString());
	    	}
	    }
	    return termList;
	}
	
	private Set<String> buildVocabulary(List<String[]> tokenizedTweets) {
		HashSet<String> v = new HashSet<String>();
		
		for(String[] tweet : tokenizedTweets ) {
			for( String token : tweet ) {
				v.add(token);
			}
		}
		
		return v;
	}
	
	public void setCaseSensitiveTerms(boolean caseSensitiveTerms) {
		this.caseSensitiveTerms = caseSensitiveTerms;
	}
	
	public void setCaseSensitiveTags(boolean caseSensitiveTags) {
		this.caseSensitiveTags = caseSensitiveTags;
	}
	
	public static void main(String[] args) {
				
		String datasetsDirectory = "/home/iman/Dropbox/Web Mining/Final Project/datasets/bowc2";
		//String datasetsDirectory = "/home/cosmin/Dropbox/Final Project/datasets/bow2";
		
		if( !Utility.dirExists(datasetsDirectory) ) {
			LOG.error("Directory does not exist: {}", datasetsDirectory);
			System.exit(1);
		}
		
		InputStream inputFile = ClassLoader.class.getResourceAsStream("/data/training-Obama-Romney-tweets.csv");
		
		BagOfWordsModel2 creator = new BagOfWordsModel2();
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.setCaseSensitiveTags(false);
		creator.setCaseSensitiveTerms(false);
		creator.createInstances();
		creator.saveArff(datasetsDirectory + File.separator + creator.getTrainingSetName() + ".arff");
		
	}

}
