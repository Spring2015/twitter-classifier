package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;

/**
 * Converts a normal training file into a two step training file.
 * 
 * 
 * @author cosmin
 *
 */
public class TwoStepConverter {

	private static Logger LOG = LogManager.getLogger(TwoStepConverter.class);
	
	public void convert(String inputFilePath) {
		
		Instances inputData = null;
		try {
			LOG.debug("Loading the ARFF file.");
			final File inputFile1 = new File(inputFilePath);
			BufferedReader reader1 = new BufferedReader(new FileReader(inputFile1));
			ArffReader arff1 = new ArffReader(reader1);
			inputData = arff1.getData();
			inputData.setClassIndex(inputData.numAttributes() - 1);
		}
		catch( IOException e) {
			LOG.error(e);
			return;
		}
		
		// create the attributes for the Instances datastructures
		FastVector attributesStep1 = new FastVector(inputData.numAttributes() - 1);
		FastVector attributesStep2 = new FastVector(inputData.numAttributes() - 1);

		for( int i = 0; i < inputData.numAttributes() - 1; i++ ) {
			Attribute newAttr1 = new Attribute(inputData.attribute(i).name());
			attributesStep1.addElement(newAttr1);
			
			Attribute newAttr2 = new Attribute(inputData.attribute(i).name());
			attributesStep2.addElement(newAttr2);
		}

		FastVector step1Labels = new FastVector();
		step1Labels.addElement("sentiment");
		step1Labels.addElement("nosentiment");

		FastVector step2Labels = new FastVector();
		step2Labels.addElement("positive");
		step2Labels.addElement("negative");

		Attribute classLabelStep1 = new Attribute("Final Class Label1", step1Labels);
		attributesStep1.addElement(classLabelStep1);

		Attribute classLabelStep2 = new Attribute("Final Class Label2", step2Labels);
		attributesStep2.addElement(classLabelStep2);
		
		// name of the training set:
		String trainingSetName1 = "step1-" + inputData.relationName();
		String trainingSetName2 = "step2-" + inputData.relationName();
		
		Instances step1Data = new Instances(trainingSetName1, attributesStep1, inputData.numInstances());
		Instances step2Data = new Instances(trainingSetName2, attributesStep2, inputData.numInstances());
		
		LOG.debug("Populating the dataset.");
		
		// populate the datasets
		final int N = inputData.numInstances();
		
		for( int i = 0; i < N; i++ ) {
			LOG.debug("Instance:\t{}/{}", i, N);
			
			Instance oldInstance = inputData.instance(i);

			Instance step1Instance = new Instance(inputData.numAttributes());
			step1Instance.setDataset(step1Data);
			
			Instance step2Instance = new Instance(inputData.numAttributes());
			step2Instance.setDataset(step2Data);
			
			// copy all the attributes (except the class label)
			for( int j = 0; j < inputData.numAttributes() - 1; j++ ) {
				step1Instance.setValue(j, oldInstance.value(j));
				step2Instance.setValue(j, oldInstance.value(j));
			}
			
			String inputClassLabel = oldInstance.stringValue(inputData.numAttributes() - 1);
			
			// set the class label for step 1 and save an instances for step 2
			if( inputClassLabel.equals("neutral") || inputClassLabel.equals("mixed") ) {
				step1Instance.setValue(inputData.numAttributes() - 1, "nosentiment");
			}
			else if ( inputClassLabel.equals("positive") || inputClassLabel.equals("negative") ) {
				step1Instance.setValue(inputData.numAttributes() - 1, "sentiment");
				
				// save the step 2 instance.
				step2Instance.setValue(inputData.numAttributes() - 1, inputClassLabel); // positive or negative
				step2Data.add(step2Instance);
			}
			else {
				throw new RuntimeException("Cannot determine input class label.");
			}
			
			step1Data.add(step1Instance);
		}
		
		
		final File file1 = new File(inputFilePath);
		
		// save the datasets
		try {
			{
				String outputFileStep1 = file1.getParent() + File.separator + step1Data.relationName() + ".arff";
				LOG.debug("Saving step 1 dataset: {}", outputFileStep1);
				ArffSaver saver1 = new ArffSaver();
				saver1.setInstances(step1Data);
				saver1.setFile(new File(outputFileStep1));
				saver1.writeBatch();
			}
			{
				String outputFileStep2 = file1.getParent() + File.separator + step2Data.relationName() + ".arff";
				LOG.debug("Saving step 2 dataset: {}", outputFileStep2);
				ArffSaver saver2 = new ArffSaver();
				saver2.setInstances(step2Data);
				saver2.setFile(new File(outputFileStep2));
				saver2.writeBatch();
			}
		}
		catch(IOException e) {
			LOG.error(e);
		}
		
	}

	public static void main(String[] args) {
		Preferences prefs = Preferences.userNodeForPackage(TwoStepConverter.class);

		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile1.getAbsolutePath());

			TwoStepConverter twoStepConverter = new TwoStepConverter();
			twoStepConverter.convert(selectedFile1.getAbsolutePath());
		}
	}
}
