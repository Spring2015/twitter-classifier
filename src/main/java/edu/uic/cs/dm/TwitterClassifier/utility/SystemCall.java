package edu.uic.cs.dm.TwitterClassifier.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Use this class to run things as if they were on the command line. 
 */
public class SystemCall {

	private static Logger LOG = LogManager.getLogger(SystemCall.class);
	
    public static String DoSystemCall(String cmd){
        try {
            Process process = Runtime.getRuntime().exec(cmd);
            
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            
            StringBuilder output = new StringBuilder();

            String currentLine;
            while( (currentLine = reader.readLine()) != null ) {
            	output.append(currentLine).append("\n");
            }
            
            return output.toString();
        } 
        catch (IOException e) {
            LOG.error(e);
            return null;
        }

    }    
}