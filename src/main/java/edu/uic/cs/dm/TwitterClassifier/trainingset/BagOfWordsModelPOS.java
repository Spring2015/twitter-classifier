package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import opennlp.tools.util.Span;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.annotation.OpenNLPPOS;

//TODO low case sensitive
public class BagOfWordsModelPOS extends AbstractTrainingSetCreator {

	private static Logger log = LogManager.getLogger(BagOfWordsModelPOS.class);
	private SentenceDetectorME sentenceDetector;
	private Tokenizer tokenizer;
	private TweetTokenizer tweetTokenizer;
	private POSTaggerME tagger;
	Map<String, Set<OpenNLPPOS>> vocabPOSTag = new HashMap<String, Set<OpenNLPPOS>>();

	public BagOfWordsModelPOS() throws Exception, IOException {
		InputStream sentModelIn = this.getClass().getResourceAsStream(
				"/opennlp/en-sent.bin");

		SentenceModel sentModel = new SentenceModel(sentModelIn);
		if (sentModelIn != null)
			sentModelIn.close();

		sentenceDetector = new SentenceDetectorME(sentModel);

		InputStream tokenModelIn = this.getClass().getResourceAsStream(
				"/opennlp/en-token.bin");
		TokenizerModel tokenModel = new TokenizerModel(tokenModelIn);
		if (tokenModelIn != null)
			tokenModelIn.close();

		tokenizer = new TokenizerME(tokenModel);

		InputStream posModelIn = this.getClass().getResourceAsStream(
				"/opennlp/en-pos-maxent.bin");
		POSModel model = new POSModel(posModelIn);
		posModelIn.close();

		tagger = new POSTaggerME(model);
	}

	/**
	 * http://bulba.sdsu.edu/jeanette/thesis/PennTags.html Part of speech tag a
	 * list of tokenized sentences.
	 */

	public List<String[]> doPartOfSpeechTag(List<String[]> tokenizedSentences) {
		ArrayList<String[]> taggedSentences = new ArrayList<String[]>();

		for (String[] currentSentence : tokenizedSentences) {
			String[] tags = tagger.tag(currentSentence);
			taggedSentences.add(tags); // WARNING: May be null.
		}

		return taggedSentences;
	}

	public void createInstances() {
		// tokenize the tweets
		List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
		// checkPOSTags();
		int counter=0;
		for (String[] tokenizedTweet : tokenizedTweets) {
			log.debug("Tweet number: "+ counter++);

			for (int i = 0; i < tokenizedTweet.length; i++) {
				log.debug("Tokenized Tweet "+i+" :"+tokenizedTweet[i]);

			}

		}

		// create the vocabulary
		 Set<String> vocabulary = buildVocabulary(vocabPOSTag);
		
		 // convert Set to an array of features.
		 String[] features = vocabulary.toArray(new String[0]);
		
		 log.debug("Found {} features.", features.length);
		
		// // create a hashmap for quick checking of string equality of features
		Map<String, Integer> featureIndexMap = new HashMap<String, Integer>();
		for (int i = 0; i < features.length; i++) {
			log.debug("vocab : " + i + " " + features[i]);
			featureIndexMap.put(features[i], new Integer(i));
		}

		FastVector attrInfo = new FastVector(features.length);
		for (String currentFeature : features) {
			Attribute currentAttribute = new Attribute(currentFeature);
			attrInfo.addElement(currentAttribute);
		}

		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");

		Attribute finalClassLabel = new Attribute("Final Class Label",
				nominalClassValues);
		attrInfo.addElement(finalClassLabel);

		instances = new Instances("twitter-bowc-pos", attrInfo, dataFile.size());
		
		for (int c = 0; c < tokenizedTweets.size(); c++) {
			String[] currentTweet = tokenizedTweets.get(c);

			log.debug("Current tweet:\t" + c + "/" + tokenizedTweets.size());

			Instance currentDataPoint = new Instance(features.length + 1); // +1
																			// because
																			// of
																			// the
																			// class
																			// label
			currentDataPoint.setDataset(instances);

			for (int i = 0; i < features.length; i++) {
				currentDataPoint.setValue(i, 0.0d);
			}

			Map<String,Integer> tokenCountMap = new HashMap<String,Integer>();
			for(String currentToken : currentTweet) {
				if( tokenCountMap.containsKey(currentToken) ) {
					Integer incrementedCount = tokenCountMap.get(currentToken) + 1;
					tokenCountMap.put(currentToken, incrementedCount);
				}
				else {
					tokenCountMap.put(currentToken, new Integer(1));
				}
			}
			
			for(String currentToken : tokenCountMap.keySet()) {
				if( featureIndexMap.containsKey(currentToken) ) {
					Integer index = featureIndexMap.get(currentToken);
					currentDataPoint.setValue(index.intValue(), (double)tokenCountMap.get(currentToken).intValue() );
				}
			}

			AnnotatedTweet aTweet = dataFile.get(c);

			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the class labels (we need this to make naive bayes work)
			currentDataPoint.setValue(features.length,
					classes[aTweet.annotationFinal + 1]); // the last feature is the class label

			instances.add(currentDataPoint);
		}
	}

	// Remove @
	// Remove retweet

	// users usually put “RT” plus the originator’s username at the beginning of
	// the tweet. Here’s an example:
	//
	// - The Twitter user @benparr tweets: I just heard that Apple is releasing
	// new iPods in July!
	//
	// - You retweet by posting RT @benparr I just heard that Apple is releasing
	// new iPods in July!

	public List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile) {
		return tokenizeTweets(dataFile, false);
	}
	
	public List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile, boolean returnTokensOnly) {

		List<String[]> tokenized = new LinkedList<String[]>();

		for( int dataFileIndex = 0; dataFileIndex < dataFile.size(); dataFileIndex++ ) {
			log.debug("Tokenizing:\t{}/{}", dataFileIndex, dataFile.size());
			
			AnnotatedTweet tweet = dataFile.get(dataFileIndex);
			
			String[] tokenizedTweet = tweetTokenizer.tokenizeTweet(tweet.tweetText);

			tokenized.add(tokenizedTweet);
		}

		return tokenized;
	}

	private boolean combineVocabAndPOSTags(String tokenTweet, String pOSTag) {

		OpenNLPPOS openNLPPOS = OpenNLPPOS.get(pOSTag);
		if (openNLPPOS == null) {
			log.debug("This tag is not in POS tag list : " + pOSTag);
			return false;
		}

		Set<OpenNLPPOS> tokenTweetPOSSet = vocabPOSTag.get(tokenTweet);
		if (tokenTweetPOSSet == null) {
			tokenTweetPOSSet = new HashSet<OpenNLPPOS>();
			tokenTweetPOSSet.add(OpenNLPPOS.get(pOSTag));
			vocabPOSTag.put(tokenTweet, tokenTweetPOSSet);

		} else {
			tokenTweetPOSSet.add(OpenNLPPOS.get(pOSTag));
			vocabPOSTag.put(tokenTweet, tokenTweetPOSSet);

		}
		return true;

	}

	/* ************************* SENTENCE DETECTION **************************** */
	/**
	 * Detect sentences from a pieces of text and return the sentence strings.
	 */
	public String[] doDetectSentences(String text) {
		return sentenceDetector.sentDetect(text);
	}

	/*
	 * Tokenize a set of sentences. For each sentence, we get an array of
	 * tokens.
	 */
	public List<String[]> doTokenize(String[] sentences) {
		ArrayList<String[]> tokenizedList = new ArrayList<String[]>();

		for (String currentSentence : sentences) {
			Span[] tokenSpans = tokenizer.tokenizePos(currentSentence);
			String[] tokenizedSentence = Span.spansToStrings(tokenSpans,
					currentSentence);
			if (tokenizedSentence != null) {
				tokenizedList.add(tokenizedSentence);
			}
		}

		return tokenizedList;
	}

	private Set<String> buildVocabulary(Map<String, Set<OpenNLPPOS>> vocabPOSTag) {
		HashSet<String> v = new HashSet<String>();

		for (Map.Entry<String, Set<OpenNLPPOS>> entry : vocabPOSTag.entrySet()) {
			String tweetToken = entry.getKey();
			Set<OpenNLPPOS> possibleTweetPosTags = entry.getValue();
//			log.debug(tweetToken + " tag size : " + possibleTweetPosTags.size()
//					+ "  " + possibleTweetPosTags);
			for (OpenNLPPOS openNLPPOS : possibleTweetPosTags) {
				v.add(tweetToken + openNLPPOS);

			}
		}

		return v;
	}

	public static void main(String[] args) {

		// String outputFile =
		// "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-simple.arff";
		String outputFile = "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-POS.arff";

		InputStream inputFile = ClassLoader.class
				.getResourceAsStream("/data/training-Obama-Romney-tweets.csv");

		BagOfWordsModelPOS creator = null;
		try {
			creator = new BagOfWordsModelPOS();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();
		 creator.saveArff(outputFile);

	}

}
