package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.Reader;
import java.util.List;

import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;

/**
 * Describes an algorithm that creates a training set from the twitter
 * annotations provided. The goal is to define the features to be used for the
 * instances.
 * 
 * @author cosmin
 * 
 */
public interface TrainingSetCreator {

	/**
	 * @param inputSource The input must be a reader to a CSV file.
	 */
	public void setInputSource(Reader inputSource);
	
	public List<AnnotatedTweet> getDataFile();
	
	public void createInstances();
	
	public Instances getInstances();
	
	/** The name of the created Instances */
	public String getTrainingSetName();
	
	/**
	 * Save the instances
	 * @param filePath
	 */
	public void saveArff(String filePath);
}
