package edu.uic.cs.dm.TwitterClassifier.TweeterTagger;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/*
 * https://github.com/brendano/ark-tweet-nlp/blob/master/docs/annot_guidelines.md
 */
public enum TwitterPOS {
	
	N("N"),
	O("O"),
	ProperN("^"),
	S("S"),
	Z("Z"),
	
	V("V"),
	A("A"),
	R("R"),
	INTER("!"),
	D("D"),
	P("P"),
	Coordi("&"),
	T("T"),
	X("X"),
	HASH("#"),
	AT("@"),
	DISCOURSE("~"),	
	E("E"),
	NUMERAL("$"),
	PUNCTUATION(","),
	OTHERABRE("G"),
	NOMINAL("L"),
	PROPERNOUNVERBAL("M"),
	XVERBAL("Y");
	//Reverse Lookup
	private static final Map<String,TwitterPOS> lookup = new HashMap<String, TwitterPOS>();
	static {
		for (TwitterPOS twitterPOS : EnumSet.allOf(TwitterPOS.class)){
			lookup.put(twitterPOS.getValue(), twitterPOS);
			
		}
	}
	
	private String pos;
	
	TwitterPOS(String subj){
		pos = subj;
	}
	
	public boolean isType(String val){
		
		if (this.toString().equals(val)) return true;
		
		return false;
	}
	
	
	public String getValue() {
		return pos;
	}
	//Reverse Lookup
	public static TwitterPOS get(String POSTag){
		return lookup.get(POSTag);
	}

}
