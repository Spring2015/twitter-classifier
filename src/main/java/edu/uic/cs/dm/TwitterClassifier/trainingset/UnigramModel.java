package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.utility.RunTimer;

/**
 * This model extracts unigrams from each tweet.
 * 
 * <ul>
 * <li>Step 1: Tokenize the tweets.</li>
 * <li>Step 2: Build a vocabulary of unigrams.</li>
 * <li>Step 3: Go through each tweet and create the feature vector.</li>
 * </ul>
 * 
 * @author Cosmin Stroe (cstroe@gmail.com)
 *
 */
public class UnigramModel extends BagOfWordsModel {
	
	private static Logger LOG = LogManager.getLogger(UnigramModel.class);

	
	public UnigramModel(String tokenizedTweetsFile, String outputFile) {
		super(tokenizedTweetsFile, outputFile);
	}
	
	public void createInstances() {
		if( tokenizedTweets == null ) {
			// Step 1. Tokenize the tweets.
			RunTimer timer = new RunTimer().start();
			List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
			timer.stop();
			LOG.debug("Tokenized {} tweets in {}.", dataFile.size(), timer.getFormattedRunTime());
			this.tokenizedTweets = tokenizedTweets;
		}
		
		createInstances(tokenizedTweets);
	}
	
	public void createInstances(List<String[]> tokenizedTweets) {
		
		RunTimer timer = new RunTimer().start();
		
		// create the vocabulary
		List<String> vocabulary = UnigramModel.buildUnigramVocabulary(tokenizedTweets);
				
		LOG.trace("Found {} features.", vocabulary.size());
				
		// create a hashmap for quick checking of string equality of features
		Map<String,Integer> featureIndexMap = new HashMap<String,Integer>();
		for(int i = 0; i < vocabulary.size(); i++) {
			featureIndexMap.put(vocabulary.get(i), new Integer(i));
		}
		
		instances = createInstancesDataset(vocabulary);
		setArffStructure(instances);
		
		// **********************************************************************
		// *** Convert the tweets to feature vectors.                         ***
		// **********************************************************************
		
		for (int c = 0; c < tokenizedTweets.size(); c++ ) {
			String[] currentTweet = tokenizedTweets.get(c);
			
			if( c % 1000 == 0 )
				LOG.debug("Current tweet:\t {}/{} --- {}", c, tokenizedTweets.size(), timer.getFormattedRunTime());
			
			
			double[] attValues = new double[vocabulary.size()+1];
			Instance currentDataPoint = new Instance(1, attValues); // +1 because of the class label
			currentDataPoint.setDataset(instances);
			
			/*// clear values
			for( int i = 0; i < currentDataPoint.numAttributes() - 1; i++ ) {
				currentDataPoint.setValue(i, 0d);
			}*/
			
			Map<String,Integer> tokenCountMap = new HashMap<String,Integer>();
			for(String currentToken : currentTweet) {
				if( tokenCountMap.containsKey(currentToken) ) {
					Integer incrementedCount = tokenCountMap.get(currentToken) + 1;
					tokenCountMap.put(currentToken, incrementedCount);
				}
				else {
					tokenCountMap.put(currentToken, new Integer(1));
				}
			}
			
			for(String currentToken : tokenCountMap.keySet()) {
				if( featureIndexMap.containsKey(currentToken) ) {
					Integer index = featureIndexMap.get(currentToken);
					currentDataPoint.setValue(index.intValue(), (double)tokenCountMap.get(currentToken).intValue() );
				}
				else {
					LOG.error("Could not find token '{}' in featureIndexMap. This should not happen.", currentToken);
				}
			}
			
			AnnotatedTweet aTweet = dataFile.get(c);
			
			//LOG.debug("\nTweet {}\n{}\n{} {}", c, Arrays.toString(currentTweet), aTweet.tweetText, aTweet.annotationFinal);
			
			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the class labels ( we need this to make naive bayes work )
			
			currentDataPoint.setValue(vocabulary.size(), classes[aTweet.annotationFinal+1]);  // the last feature is the class label
			
			writeInstance(currentDataPoint);
			
		}
		
		closeWriter();
		timer.stop();
		LOG.debug("Runtime: {}", timer.getFormattedRunTime());
	}
	
	/**
	 * Create a vocabulary of unigrams.
	 */
	public static List<String> buildUnigramVocabulary(List<String[]> tokenizedTweets) {
		Set<String> vocab = new HashSet<String>();
		
		for(String[] currentTweet : tokenizedTweets) {
			for( String currentToken : currentTweet ) {
				vocab.add(currentToken);
			}
		}

		return new ArrayList<String>(vocab);
	}
	
	private Instances createInstancesDataset(List<String> unigramList) {
		
		FastVector attrInfo = new FastVector(unigramList.size() + 1); // +1 for the class label
		for( String currentFeature : unigramList ) {
			Attribute currentAttribute = new Attribute(currentFeature);
			attrInfo.addElement(currentAttribute);
		}
				
		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");
		
		
		Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
		attrInfo.addElement(finalClassLabel);
		
		// name of the training set:
		String trainingSetName = "unigram";
				
		return new Instances(trainingSetName, attrInfo, dataFile.size());
	}
	
	public static void main(String[] args) throws IOException {
			
		createSet("Obama"   , "training");
		createSet("Romney"  , "training");
		createSet("Together", "training");
		
		createSet("Obama"   , "testing");
		createSet("Romney"  , "testing");
		createSet("Together", "testing");
	}
	
	/**
	 * 
	 * @param process This string must be one of these (case sensitive): Obama, Romney, or Together
	 * @param set This string must be one of these: training, testing
	 */
	public static void createSet(String process, String set) {
		
		InputStream inputFile = ClassLoader.class.getResourceAsStream("/data/"+set+"-"+process+"-tweets.csv");
		
		UnigramModel creator = new UnigramModel(
				"/home/cosmin/Dropbox/Final Project/datasets/report/"+set+"-tokenized-tweets-"+process.toLowerCase()+".ser.bz2",
				"/home/cosmin/Dropbox/Final Project/datasets/report/"+set+"/"+set+"-"+process.toLowerCase()+"-unigram.arff.gz");
		
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();	
	}
}
