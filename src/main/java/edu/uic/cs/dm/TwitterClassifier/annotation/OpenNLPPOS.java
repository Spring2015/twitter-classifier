package edu.uic.cs.dm.TwitterClassifier.annotation;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/*
 * http://bulba.sdsu.edu/jeanette/thesis/PennTags.html
 */
public enum OpenNLPPOS {
	
	CC("CC"),
	CD("CD"),
	DT("DT"),
	EX("EX"),
	FW("FW"),
	IN("IN"),
	JJ("JJ"),
	JJR("JJR"),
	JJS("JJS"),
	LS("LS"),
	MD("MD"),
	NN("NN"),
	NNS("NNS"),
	NNSS("NNSS"),
	NNP("NNP"),
	NNPS("NNPS"),
	PDT("PDT"),
	POS("POS"),	
	PRP("PRP"),
	PRP$("PRP$"),
	RB("RB"),
	RBR("RBR"),
	RBS("RBS"),
	RP("RP"),
	SYM("SYM"),
	TO("TO"),
	UH("UH"),
	VB("VB"),
	VBD("VBD"),
	VBG("VBG"),
	VBN("VBN"),
	VBP("VBP"),
	VBZ("VBZ"),
	WDT("WDT"),
	WP("WP"),
	WP$("WP$"),
	WRB$("WRB"),
	empty("empty");
	//Reverse Lookup
	private static final Map<String,OpenNLPPOS> lookup = new HashMap<String, OpenNLPPOS>();
	static {
		for (OpenNLPPOS nlppos: EnumSet.allOf(OpenNLPPOS.class)){
			lookup.put(nlppos.getValue(), nlppos);
			
		}
	}
	
	private String pos;
	
	OpenNLPPOS(String subj){
		pos = subj;
	}
	
	public boolean isType(String val){
		
		if (this.toString().equals(val)) return true;
		
		return false;
	}
	
	
	public String getValue() {
		return pos;
	}
	//Reverse Lookup
	public static OpenNLPPOS get(String POSTag){
		return lookup.get(POSTag);
	}

}
