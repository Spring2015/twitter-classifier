package edu.uic.cs.dm.TwitterClassifier.trainingset.arff;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ArffLoader.ArffReader;

public class ArffCombiner {
	
	private static Logger LOG = LogManager.getLogger(ArffCombiner.class);

	private void combine(String absolutePath1, String absolutePath2) {
		try {
			LOG.debug("Loading first ARFF file.");
			final File inputFile1 = new File(absolutePath1);
			BufferedReader reader1 = new BufferedReader(new FileReader(inputFile1));
			ArffReader arff1 = new ArffReader(reader1);
			Instances data1 = arff1.getData();
			data1.setClassIndex(data1.numAttributes() - 1);
			
			LOG.debug("Loading second ARFF file.");
			final File inputFile2 = new File(absolutePath2);
			BufferedReader reader2 = new BufferedReader(new FileReader(inputFile2));
			ArffReader arff2 = new ArffReader(reader2);
			Instances data2 = arff2.getData();
			data2.setClassIndex(data2.numAttributes() - 1);
			
			if( data1.numInstances() != data2.numInstances() ) {
				throw new RuntimeException("Datasets do not have the same number of instances.");
			}
			
			// create the attributes for the Instances datastructure
			FastVector attrInfo = new FastVector(data1.numAttributes() + data2.numAttributes() - 1);
			
			for( int i = 0; i < data1.numAttributes() - 1; i++ ) {
				Attribute newAttr = new Attribute(data1.attribute(i).name());
				attrInfo.addElement(newAttr);
			}
			
			for( int i = 0; i < data2.numAttributes() - 1; i++ ) {
				Attribute newAttr = new Attribute(data2.attribute(i).name());
				attrInfo.addElement(newAttr);
			}
			
			FastVector nominalClassValues = new FastVector();
			nominalClassValues.addElement("negative");
			nominalClassValues.addElement("neutral");
			nominalClassValues.addElement("positive");
			nominalClassValues.addElement("mixed");
			
			
			Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
			attrInfo.addElement(finalClassLabel);
			
			// name of the training set:
			String trainingSetName = "merged-" + data1.relationName() + "-" + data2.relationName();
			Instances mergedData = new Instances(trainingSetName, attrInfo, data1.numInstances());
			
			
			LOG.debug("Populating the dataset.");
			
			for( int i = 0; i < data1.numInstances(); i++ ) {
				LOG.debug("Instance:\t{}/{}", i, data1.numInstances());
				Instance data1Instance = data1.instance(i);
				Instance data2Instance = data2.instance(i);
				
				Instance mergedInstance = new Instance(mergedData.numAttributes());
				mergedInstance.setDataset(mergedData);
				
				int j = 0;
				for( int k = 0; k < data1.numAttributes() - 1; k++, j++) {
					mergedInstance.setValue(j, data1Instance.value(k));
				}
				for( int k = 0; k < data2.numAttributes() - 1; k++, j++) {
					mergedInstance.setValue(j, data2Instance.value(k));
				}
				
				//j++;
				mergedInstance.setValue(j, data1Instance.stringValue(data1.numAttributes() - 1));
				
				mergedData.add(mergedInstance);
			}
						
			final File file1 = new File(absolutePath1);
			String outputFile = file1.getParent() + File.separator + mergedData.relationName() + ".arff";
			LOG.debug("Saving new dataset: {}", outputFile);

			ArffSaver saver = new ArffSaver();
			saver.setInstances(mergedData);
			saver.setFile(new File(outputFile));
			saver.writeBatch();
		}
		catch(IOException e) {
			LOG.error(e);
		}
		
	}
	
	
	public static void main(String[] args) {
		Preferences prefs = Preferences.userNodeForPackage(ArffCombiner.class);

		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile1.getAbsolutePath());
			if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
				File selectedFile2 = fc.getSelectedFile();
				ArffCombiner arffCombiner= new ArffCombiner();
				arffCombiner.combine(selectedFile1.getAbsolutePath(), selectedFile2.getAbsolutePath());
			}
		}

	}

	

}
