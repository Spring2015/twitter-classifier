From http://opennlp.sourceforge.net/models-1.5/

en 	Tokenizer 	Trained on opennlp training data. 	en-token.bin
en 	Sentence Detector 	Trained on opennlp training data. 	en-sent.bin
en 	POS Tagger 	Maxent model with tag dictionary. 	en-pos-maxent.bin
en 	POS Tagger 	Perceptron model with tag dictionary. 	en-pos-perceptron.bin
en 	Name Finder 	Date name finder model. 	en-ner-date.bin
en 	Name Finder 	Location name finder model. 	en-ner-location.bin
en 	Name Finder 	Money name finder model. 	en-ner-money.bin
en 	Name Finder 	Organization name finder model. 	en-ner-organization.bin
en 	Name Finder 	Percentage name finder model. 	en-ner-percentage.bin
en 	Name Finder 	Person name finder model. 	en-ner-person.bin
en 	Name Finder 	Time name finder model. 	en-ner-time.bin
en 	Chunker 	Trained on conll2000 shared task data. 	en-chunker.bin
en 	Parser 		en-parser-chunking.bin
en 	Coreference 		coref
