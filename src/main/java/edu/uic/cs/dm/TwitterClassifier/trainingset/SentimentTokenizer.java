package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.python.core.Py;
import org.python.core.PyInstance;
import org.python.core.PyObject;
import org.python.core.PyString;
import org.python.core.PySystemState;
import org.python.util.PythonInterpreter;

import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.trainingset.AbstractTrainingSetCreator.SubString;

public class SentimentTokenizer extends AbstractTrainingSetCreator {

	/**
	 * @param args
	 */
	PythonInterpreter interpreter = null;
	private static Logger LOG = LogManager
			.getLogger(SentimentTokenizer.class);

	public static String doSystemCall(String cmd) {
		StringBuilder output = new StringBuilder();
		try {// "chmod", "u"+('+')+"x",

			Process p = Runtime.getRuntime().exec(
					"python " + "tokenizing.py " + cmd);
			BufferedReader input = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader error = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			String line = "";
			Boolean flag = true;
			while ((line = input.readLine()) != null) {
				// deleting tweet text
				if (flag) {
					flag = false;
					continue;
				}
				output.append(line + ", ");
			}
		} catch (IOException e) {

			LOG.error(e);
		}
		return output.toString();
	}

	public static void main(String[] args) {

		// String outputFile =
		// "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-simple.arff";
		// String outputFile =
		// "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-POS.arff";

		InputStream inputFile = ClassLoader.class
				.getResourceAsStream("/data/training-Obama-Romney-tweets.csv");

		SentimentTokenizer creator = null;
		try {
			creator = new SentimentTokenizer();

		} catch (Exception e) {
			LOG.error(e);
		}
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();
		// creator.saveArff(outputFile);

	}

	public void createInstances() {
		List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
		// checkPOSTags();
		int counter = 0;
		for (String[] tokenizedTweet : tokenizedTweets) {
			LOG.debug("Tweet number: " + counter++);

			for (int i = 0; i < tokenizedTweet.length; i++) {
				LOG.debug("Tokenized Tweet " + i + " :" + tokenizedTweet[i]);

			}

		}

	}

	private List<String[]> tokenizeTweets(List<AnnotatedTweet> dataFile) {
		List<String[]> tokenized = new LinkedList<String[]>();

		for (int dataFileIndex = 0; dataFileIndex < dataFile.size(); dataFileIndex++) {
			LOG.debug("Tokenizing:\t{}/{}", dataFileIndex, dataFile.size());
			AnnotatedTweet tweet = dataFile.get(dataFileIndex);

			String cleanTweet = removeTweetAnnotations(tweet.tweetText);

			SubString[] hashTags = extractSubString(cleanTweet, hashTagPattern);
			cleanTweet = clearSubStrings(cleanTweet, hashTags);

			SubString[] urls = extractSubString(cleanTweet, urlPattern);
			cleanTweet = clearSubStrings(cleanTweet, urls);

			SubString[] ats = extractSubString(cleanTweet, atPattern);
			cleanTweet = clearSubStrings(cleanTweet, ats);

			SubString[] reTweet = extractSubString(cleanTweet, reTweetPattern);
			cleanTweet = clearSubStrings(cleanTweet, reTweet);

			LOG.debug(doSystemCall(cleanTweet));
			LOG.debug("");

		}

		return tokenized;
	}

}
