package edu.uic.cs.dm.TwitterClassifier.trainingset.arff;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader.ArffReader;
import weka.core.converters.ArffSaver;
import weka.core.converters.Saver;
import edu.uic.cs.dm.TwitterClassifier.Utility;
import edu.uic.cs.dm.TwitterClassifier.utility.RunTimer;

public class ArffAttributeMatcher {

	private static Logger LOG = LogManager.getLogger(ArffAttributeMatcher.class);
	
	
	private void merge(String referencePath, String absolutePath2) {
		
		try {
			RunTimer timer = new RunTimer().start();
			
			LOG.debug("Opening first ARFF file.");
			final File referenceFile = new File(referencePath);
			
			InputStream is1 = Utility.openCompressedFile(referenceFile, null);
			BufferedReader referenceReader = new BufferedReader(new InputStreamReader(is1));
			ArffReader referenceArffReader = new ArffReader(referenceReader, 1);
			Instances referenceHeader = referenceArffReader.getStructure();
			
			//Instances referenceData = referenceArff.getData();
			//referenceData.setClassIndex(referenceData.numAttributes() - 1);
			
			LOG.debug("Opening second ARFF file.");
			final File inputFile2 = new File(absolutePath2);
			InputStream is2 = Utility.openCompressedFile(inputFile2, null);
			BufferedReader reader2 = new BufferedReader(new InputStreamReader(is2));
			ArffReader dataArffReader = new ArffReader(reader2, 1);
			Instances dataHeader = dataArffReader.getStructure();
			
			// Instances data2 = arff2.getData();
			// data2.setClassIndex(data2.numAttributes() - 1);
			
			LOG.debug("Creating merged attributes...");
			
			//Set<String> referenceAttributeNames = new HashSet<String>();
/*			
			{
				Enumeration referenceAttributes = referenceData.enumerateAttributes();
				while( referenceAttributes.hasMoreElements() ) {
					Attribute currentAttribute = (Attribute) referenceAttributes.nextElement();
					if( currentAttribute.name().equals("Final Class Label") ) continue; // skip the class label
					referenceAttributeNames.add(currentAttribute.name());
				}
			}*/
			
			// create the attributes for the Instances datastructure
			FastVector alignedAttributes = new FastVector(referenceHeader.numAttributes());
			
			for( int i = 0; i < referenceHeader.numAttributes() - 1; i++) {
				Attribute newAttr = new Attribute(referenceHeader.attribute(i).name());
				alignedAttributes.addElement(newAttr);
			}
			
			FastVector nominalClassValues = new FastVector();
			nominalClassValues.addElement("negative");
			nominalClassValues.addElement("neutral");
			nominalClassValues.addElement("positive");
			nominalClassValues.addElement("mixed");
			
			
			Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
			alignedAttributes.addElement(finalClassLabel);
			
			String trainingSetName = "aligned-" + dataHeader.relationName();
			Instances alignedData = new Instances(trainingSetName, alignedAttributes, 1);
			
			ArffSaver arffSaver = new ArffSaver();
			arffSaver.setRetrieval(Saver.INCREMENTAL);
			arffSaver.setStructure(alignedData);
			
			OutputStream os = null;
			try {
				String dirPath = FilenameUtils.getFullPath(absolutePath2);
				String fileName = FilenameUtils.getName(absolutePath2);
				os = 
						Utility.writeCompressedFile(new File(dirPath + "aligned-" + fileName), null);
				arffSaver.setDestination(os);
			}
			catch( IOException io ) {
				io.printStackTrace();
				arffSaver = null;
			}
			
			
			LOG.debug("Creating maps...");
			
			Map<Integer,Integer> data2toReference = new HashMap<Integer,Integer>();
			
			for( int i = 0; i < alignedAttributes.size(); i++ ) {
				Attribute alignedAttr = (Attribute) alignedAttributes.elementAt(i);
				Enumeration data2Attributes = dataHeader.enumerateAttributes();
				for( int j = 0; data2Attributes.hasMoreElements(); j++ ) {
					Attribute data2Attribute = (Attribute) data2Attributes.nextElement();
					
					if( alignedAttr.name().equals(data2Attribute.name()) ) {
						data2toReference.put(new Integer(j), new Integer(i));
					}
				}
			}
			
			Instance currentData2Instance;
			int i = 1;
			while( (currentData2Instance = dataArffReader.readInstance(dataHeader)) != null) {
				
				LOG.debug("Converting instance {}",i++);
				
				Instance newInstance = new Instance(alignedAttributes.size());
				newInstance.setDataset(alignedData);
				
				// clear the new instance
/*				for( int j = 0; j < newInstance.numAttributes() - 1; j++ ) {
					newInstance.setValue(j, 0d);
				}*/
				
				// fill in the new instance
				for( int j = 0; j < currentData2Instance.numAttributes() - 1; j++ ) {
					Integer mapped = data2toReference.get(new Integer(j));
					if( mapped == null ) continue;
					newInstance.setValue(mapped.intValue(), currentData2Instance.value(j));
				}
				
				int lastAttribute = newInstance.numAttributes() - 1;
				int lastAttribute2 = currentData2Instance.numAttributes() - 1;
				String lastAttributeValue = currentData2Instance.stringValue(lastAttribute2);
				newInstance.setValue(lastAttribute, lastAttributeValue);
				
				arffSaver.writeIncremental(newInstance);
			}
			
			os.flush();
			os.close();
			
			timer.stop();
			LOG.debug("Runtime {}.", timer.getFormattedRunTime());
		}
		catch( IOException e ) {
			LOG.error(e);
		}
		
	}
	
	
	public static void main(String[] args) {
		Preferences prefs = Preferences.userNodeForPackage(ArffAttributeMatcher.class);

		JFileChooser fc = new JFileChooser();
		fc.setSelectedFile(new File(prefs.get("LASTDIR1", "")));
		if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR1", selectedFile1.getAbsolutePath());
			
			fc.setSelectedFile(new File(prefs.get("LASTDIR2", "")));
			if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
				File selectedFile2 = fc.getSelectedFile();
				prefs.put("LASTDIR2", selectedFile2.getAbsolutePath());
				ArffAttributeMatcher arffMatcher = new ArffAttributeMatcher();
				arffMatcher.merge(selectedFile1.getAbsolutePath(), selectedFile2.getAbsolutePath());
			}
		}

	}
	
}
