package edu.uic.cs.dm.TwitterClassifier.analysis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.prefs.Preferences;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JFileChooser;

import org.apache.commons.math3.stat.inference.TestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Perform a significance test between two classifier outputs.
 * 
 * @author cosmin
 *
 */
public class ChiSquareSignificanceTest {

	private static Logger LOG = LogManager.getLogger(ChiSquareSignificanceTest.class);
		
	public double chiSquare(String outputFile1, String outputFile2) {
		EvaluatedInstance[] output1 = parseOutputFile(outputFile1);
		EvaluatedInstance[] output2 = parseOutputFile(outputFile2);
		
		if( output1.length != output2.length ) {
			throw new RuntimeException("The length of the evaluations do not match.");
		}
		
		for( int i = 0; i < output1.length; i++ ) {
			EvaluatedInstance inst1 = output1[i];
			EvaluatedInstance inst2 = output2[i];
			
			if( inst1.instanceNum != inst2.instanceNum ) {
				throw new RuntimeException("The instance numbers do not match. " + inst1.instanceNum + "-" + inst2.instanceNum);
			}
			
			if( inst1.actualEvaluation != inst2.actualEvaluation ) {
				throw new RuntimeException("The actual evaluations of the instances do not match. " + inst1.instanceNum + "-" + inst2.instanceNum);
			}
			
		}
		
		long[] observed1 = new long[output1.length];
		long[] observed2 = new long[output2.length];
		
		for( int j = 0; j < output1.length; j++ ) {
			observed1[j] = output1[j].predictedEvaluation;
			observed2[j] = output2[j].predictedEvaluation;
		}
				
		double chiSquare = TestUtils.chiSquareDataSetsComparison(observed1, observed2);
		
		LOG.debug("ChiSquare: {}", chiSquare);
		
		return chiSquare;
	}
	
	private Pattern instancesPattern = Pattern.compile("Instances:\\s+([0-9]+)");
	
	private Pattern evaluationPattern = Pattern.compile("\\s+([0-9]+)\\s+([1-4]):[a-z]+\\s+([1-4]):[a-z]+\\s+");
	
	private EvaluatedInstance[] parseOutputFile(String file) {
		
		int numInstances = 0;
		
		List<EvaluatedInstance> instanceList = new LinkedList<EvaluatedInstance>();
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			
			String currentLine;
			boolean flagSearchForAttributes = false, flagReadInstanceEvaluations = false;
			while( (currentLine = reader.readLine()) != null ) {
				if( currentLine.startsWith("=== Run information ===") ) {
					flagSearchForAttributes = true;
					currentLine = reader.readLine();
				}
					
				if( currentLine.startsWith("=== Predictions on test data ===") ) {
					flagReadInstanceEvaluations = true;
					currentLine = reader.readLine();
				}
				
				if( flagSearchForAttributes ) {
					Matcher m = instancesPattern.matcher(currentLine);
					if( m.find() ) {
						numInstances = Integer.parseInt(m.group(1));
						flagSearchForAttributes = false;
					}
					if( currentLine.startsWith("===") ) {
						flagSearchForAttributes = false;
					}
				}
				
				if( flagReadInstanceEvaluations ) {
					Matcher m = evaluationPattern.matcher(currentLine);
					if( m.find() ) {
						final int instanceNum = Integer.parseInt(m.group(1));
						final int actual = Integer.parseInt(m.group(2));
						final int predicted = Integer.parseInt(m.group(3));
						instanceList.add(new EvaluatedInstance(instanceNum, actual, predicted));
					}
					if( currentLine.startsWith("===") ) {
						flagReadInstanceEvaluations = false;
					}
				}
			}
			
			reader.close();
		} catch (FileNotFoundException e) {
			LOG.error(e);
		} catch (IOException e) {
			LOG.error(e);
		}
		
		if( instanceList.size() != numInstances ) {
			throw new RuntimeException("The number of instances found does not match the stated number.");
		}
		
		return instanceList.toArray(new EvaluatedInstance[0]);
	}
	
	
	private static class EvaluatedInstance {
		final public int instanceNum;
		final public int actualEvaluation;
		final public int predictedEvaluation;
		
		public EvaluatedInstance(int instanceNum, int actualEvaluation, int predictedEvaluation) {
			this.instanceNum = instanceNum;
			this.actualEvaluation = actualEvaluation;
			this.predictedEvaluation = predictedEvaluation;
		}
	}
	
	
	public static void main(String[] args) {
		
		Preferences prefs = Preferences.userNodeForPackage(ChiSquareSignificanceTest.class);
		
		JFileChooser fc = new JFileChooser(prefs.get("LASTDIR", null));
		if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
			File selectedFile1 = fc.getSelectedFile();
			prefs.put("LASTDIR", selectedFile1.getAbsolutePath());
			if( fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
				File selectedFile2 = fc.getSelectedFile();
				ChiSquareSignificanceTest chiSquare = new ChiSquareSignificanceTest();
				chiSquare.chiSquare(selectedFile1.getAbsolutePath(), selectedFile2.getAbsolutePath());
			}
		}
		
	}
}
