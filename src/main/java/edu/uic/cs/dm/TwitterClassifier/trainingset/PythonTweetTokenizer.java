package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.Normalizer;
import java.util.ArrayList;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.trainingset.AbstractTrainingSetCreator.SubString;

public class PythonTweetTokenizer implements TweetTokenizer {
	
	// avoid reallocating a new array every time.
	private static ArrayList<String> recycledArray = new ArrayList<String>(1000);
	
	private boolean normalizeUnicode = true;
	
	private boolean caseSensitive = false;
	
	private boolean keepHashTags = true;
	
	/**
	 * @param args
	 */
	//PythonInterpreter interpreter = null;
	private static Logger LOG = LogManager.getLogger(PythonTweetTokenizer.class);
	
	public String[] doTokenizeByPythonSystemCall(String tweetText) {
		
		// this list will hold the tokenized tweet
		recycledArray.clear();

		try {

			Process p = Runtime.getRuntime().exec(
					"python " + "tokenizing.py " + tweetText);
			
			BufferedReader stdout = new BufferedReader(new InputStreamReader(
					p.getInputStream()));

			BufferedReader stderr = new BufferedReader(new InputStreamReader(
					p.getErrorStream()));

			String stderrLine;
			StringBuilder outputErrorForTweet = new StringBuilder();
			while( (stderrLine = stderr.readLine()) != null ) {
				outputErrorForTweet.append(stderrLine);
			}

			if (outputErrorForTweet.length() > 0) {
				LOG.debug("Error for this tweet: " + tweetText + "    +"
						+ outputErrorForTweet);
				return null;
			}

			String line;
			boolean skipFirstLine = true;
			while( (line = stdout.readLine()) != null ) {
				// ignoring the first line (tweet text)
				if (skipFirstLine) {
					skipFirstLine = false;
					continue;
				}
				
				if( normalizeUnicode ) {
					line = Normalizer.normalize(line, Normalizer.Form.NFD);
				}
				
				if( !caseSensitive ) {
					line = line.toLowerCase();
				}
				
				recycledArray.add(line);
			}
		} catch (IOException e) {
			LOG.error(e);
			return null;
		}
		return recycledArray.toArray(new String[0]);
	}

	//@Override
	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}
	
	//@Override
	public void setNormalizeUnicode(boolean normalizeUnicode) {
		this.normalizeUnicode = normalizeUnicode;
	}
	
	//@Override
	public void setKeepHashTags(boolean keepHashTags) {
		this.keepHashTags = keepHashTags;
	}

	/**
	 * Helper function.
	 */
	public String[] tokenizeTweet(AnnotatedTweet tweet) {
		return tokenizeTweet(tweet.tweetText);
	}
	
	//@Override
	public String[] tokenizeTweet(String tweetText) {
		String cleanTweet = AbstractTrainingSetCreator.removeTweetAnnotations(tweetText);

		if( !keepHashTags ) {
			SubString[] hashTags = AbstractTrainingSetCreator.extractSubString(cleanTweet, AbstractTrainingSetCreator.hashTagPattern);
			cleanTweet = AbstractTrainingSetCreator.clearSubStrings(cleanTweet, hashTags);
		}

		SubString[] urls = AbstractTrainingSetCreator.extractSubString(cleanTweet, AbstractTrainingSetCreator.urlPattern);
		cleanTweet = AbstractTrainingSetCreator.clearSubStrings(cleanTweet, urls);

		SubString[] ats = AbstractTrainingSetCreator.extractSubString(cleanTweet, AbstractTrainingSetCreator.atPattern);
		cleanTweet = AbstractTrainingSetCreator.clearSubStrings(cleanTweet, ats);

		SubString[] reTweet = AbstractTrainingSetCreator.extractSubString(cleanTweet, AbstractTrainingSetCreator.reTweetPattern);
		cleanTweet = AbstractTrainingSetCreator.clearSubStrings(cleanTweet, reTweet);

		// Escape Quote
		cleanTweet = StringEscapeUtils.escapeJava(cleanTweet);
		
		String[] tokenizedTweet = doTokenizeByPythonSystemCall(cleanTweet);
		
		return tokenizedTweet;
	}

}
