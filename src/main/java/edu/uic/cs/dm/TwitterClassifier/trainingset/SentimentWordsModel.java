package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.Utility;

public class SentimentWordsModel extends AbstractTrainingSetCreator {

	private static Logger LOG = LogManager.getLogger(SentimentWordsModel.class); 
	
	public void createInstances() {
		
		// load the positive and negative word lists.
		Set<String> posWordList, negWordList;
		try {
			posWordList = readWordList(ClassLoader.class.getResourceAsStream("/data/positive-words.txt"));
			negWordList = readWordList(ClassLoader.class.getResourceAsStream("/data/negative-words.txt"));
		}
		catch( IOException e ) {
			LOG.error(e); return;
		}
		
		BagOfWordsModelPOS posModel = null;
		try {
			posModel = new BagOfWordsModelPOS();
		} catch (IOException e) {
			LOG.error(e); return;
		} catch (Exception e) {
			LOG.error(e); return;
		}
		
		List<String[]> tokenizedTweets = posModel.tokenizeTweets(dataFile, true);
		
		final String[] sentimentWordFeatures = { "negative-word-list-match", "positive-word-list-match" };
		
		
		// create the attributes for the Instances datastructure
		FastVector attrInfo = new FastVector(sentimentWordFeatures.length);
		for( String currentFeature : sentimentWordFeatures ) {
			Attribute currentAttribute = new Attribute(currentFeature);
			attrInfo.addElement(currentAttribute);
		}
		
		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");
		
		
		Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
		attrInfo.addElement(finalClassLabel);
		
		// name of the training set:
		String trainingSetName = "bow-sentimentWordListMatch";
		instances = new Instances(trainingSetName, attrInfo, dataFile.size());
		
		for (int c = 0; c < tokenizedTweets.size(); c++ ) {
			String[] currentTweet = tokenizedTweets.get(c);
			
			LOG.debug("Current tweet:\t" + c + "/" + tokenizedTweets.size());
			
			Instance currentDataPoint = new Instance(sentimentWordFeatures.length+1); // +1 because of the class label
			currentDataPoint.setDataset(instances);
				
			int negWordCount = 0, posWordCount = 0;
			
			for(String currentToken : currentTweet) {
				if( negWordList.contains(currentToken) ) negWordCount++;
				if( posWordList.contains(currentToken) ) posWordCount++;
			}
			
			currentDataPoint.setValue(0, (double)negWordCount);
			currentDataPoint.setValue(1, (double)posWordCount);
						
			AnnotatedTweet aTweet = dataFile.get(c);
			
			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the class labels ( we need this to make naive bayes work )
			
			currentDataPoint.setValue(sentimentWordFeatures.length, classes[aTweet.annotationFinal+1]);  // the last feature is the class label
			
			instances.add(currentDataPoint);
		}
		
	}
	
	private Set<String> readWordList(InputStream wordList) throws IOException {
		Set<String> words = new HashSet<String>();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(wordList));
		
		String currentLine;
		while( (currentLine = reader.readLine()) != null ) {
			if( currentLine.startsWith(";") || currentLine.trim().length() == 0 ) continue;
			words.add(currentLine.trim());
		}
		
		reader.close();
		
		return words;
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		//String outputFile = "/home/iman/Dropbox/Web Mining/Final Project/datasets/bow-simple.arff";
		String datasetsDirectory = "/home/cosmin/Dropbox/Final Project/testing";
		
		if( !Utility.dirExists(datasetsDirectory) ) {
			LOG.error("Directory does not exist: {}", datasetsDirectory);
			System.exit(1);
		}
		
		//InputStream inputFile = ClassLoader.class.getResourceAsStream("/data/training-Romney-tweets.csv");
		InputStream inputFile = new FileInputStream(new File("/home/cosmin/Dropbox/Final Project/testing/testing-Romney-tweets.csv"));
		
		SentimentWordsModel creator = new SentimentWordsModel();
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();
		
		creator.saveArff(datasetsDirectory + File.separator + "romney" + creator.getTrainingSetName() + ".arff");
		
	}

}
