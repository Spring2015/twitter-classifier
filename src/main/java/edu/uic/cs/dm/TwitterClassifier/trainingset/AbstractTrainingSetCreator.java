package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.io.Reader;
import java.util.Arrays;
import java.util.BitSet;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import au.com.bytecode.opencsv.CSVReader;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;

public abstract class AbstractTrainingSetCreator implements TrainingSetCreator {

	private final static int INDEX_TWEET_NUM = 0;
	private final static int INDEX_TWEET_DATE = 1;
	private final static int INDEX_TWEET_TIME = 2;
	private final static int INDEX_TWEET_TEXT = 3;
	private final static int INDEX_ANNOTATION_1 = 4;
	private final static int INDEX_ANNOTATION_2 = 5;
	private final static int INDEX_ANNOTATION_FINAL = 6;
	
	private static Logger LOG = LogManager.getLogger(AbstractTrainingSetCreator.class);
	
	protected Instances instances;
	
	protected List<AnnotatedTweet> dataFile;
	
	public static final Pattern hashTagPattern = Pattern.compile("#[a-zA-Z0-9_\\-\\.]+");
	public static final Pattern urlPattern = Pattern.compile("http://[a-zA-Z0-9]+\\.[a-zA-Z]+/[a-zA-Z0-9]+");
	public static final Pattern atPattern = Pattern.compile("@[a-zA-Z0-9]+");
	public static final Pattern reTweetPattern = Pattern.compile("\\s+RT\\s+");
	
	/**
	 * Parse the training file.
	 */
	public void setInputSource(Reader csvFileReader) {
		try {
			dataFile = new LinkedList<AnnotatedTweet>();
			CSVReader reader = new CSVReader(csvFileReader, ',', '"');
			String[] currentLine;
			int a1, a2, af;
			while( (currentLine = reader.readNext()) != null ) {
				// check to see if we have a valid tweet id
				try {
					Integer.parseInt(currentLine[INDEX_TWEET_NUM].trim());
					a1 = Integer.parseInt(currentLine[INDEX_ANNOTATION_1].trim());
					a2 = Integer.parseInt(currentLine[INDEX_ANNOTATION_2].trim());
					af = Integer.parseInt(currentLine[INDEX_ANNOTATION_FINAL].trim());
				}
				catch( NumberFormatException e ) {
					LOG.debug("Skipping line: {}", Arrays.toString(currentLine));
					//LOG.debug(e);
					continue;
				}
				
				final AnnotatedTweet newTweet = new AnnotatedTweet(currentLine[INDEX_TWEET_DATE], currentLine[INDEX_TWEET_TIME], currentLine[INDEX_TWEET_TEXT], a1, a2, af, new BitSet());
				dataFile.add(newTweet);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		LOG.trace("End of setInputSource.");
	}
	
	public Instances getInstances() {
		return instances;
	}
	
	public String getTrainingSetName() {
		return instances.relationName();
	}
	
	public List<AnnotatedTweet> getDataFile() {
		return dataFile;
	}

	public void saveArff(String filePath) {
		ArffSaver saver = new ArffSaver();
		saver.setInstances(instances);
		try {
			saver.setFile(new File(filePath));
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveArff(OutputStream outputStream) {
		ArffSaver saver = new ArffSaver();
		saver.setInstances(instances);
		try {
			saver.setDestination(outputStream);
			saver.writeBatch();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String removeTweetAnnotations(String tweetText) {
		return tweetText
				.replaceAll("<[eEaA]>", " ")
				.replaceAll("</[eEaA]>", " ")
				.replaceAll("<[aAeE]/>", " ") // typo: <a/>
				.trim();
	}

	/**
	 * Find all the substrings in the current text.
	 * @param tweetText
	 * @return
	 */
	public static SubString[] extractSubString(String tweetText, Pattern substringPattern) {
		
		List<SubString> ssList = new LinkedList<SubString>();
		
		Matcher htMatcher = substringPattern.matcher(tweetText);
		while( htMatcher.find() ) {
			final String text = htMatcher.group();
			final int start = htMatcher.start();
			final int end = htMatcher.end();
			
			ssList.add(new SubString(text,start,end));
		}
		
		return ssList.toArray(new SubString[0]);
	}
	
	
	public static String clearSubStrings(String text, SubString[] substrings) {
		for(SubString ss : substrings) {
			String whitespace = StringUtils.repeat(" ", ss.text.length());
			text = text.substring(0, ss.start) + whitespace + text.substring(ss.end, text.length());
		}
		
		return text;
	}
	
	public static class SubString {
		/** The substring text. */
		final String text;
		
		/** The start offset of the substring in the original string. */
		final int start;
		
		/** The end offset of the substring in the original string. */
		final int end;
		
		public SubString(String text, int start, int end) {
			this.text = text;
			this.start = start;
			this.end = end;
		}
	}
}
