package edu.uic.cs.dm.TwitterClassifier.trainingset;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import edu.uic.cs.dm.TwitterClassifier.AnnotatedTweet;
import edu.uic.cs.dm.TwitterClassifier.Utility;
import edu.uic.cs.dm.TwitterClassifier.utility.RunTimer;

public class BigramModel extends BagOfWordsModel {

	private static Logger LOG = LogManager.getLogger(BigramModel.class); 
	
	public BigramModel(String tokenizedTweetsFile, String outputFile) {
		super(tokenizedTweetsFile, outputFile);
	}
	
	//@Override
	public void createInstances() {
		if( tokenizedTweets == null ) {
			// Step 1. Tokenize the tweets.
			RunTimer timer = new RunTimer().start();
			List<String[]> tokenizedTweets = tokenizeTweets(dataFile);
			timer.stop();
			LOG.debug("Tokenized {} tweets in {}.", dataFile.size(), timer.getFormattedRunTime());
			this.tokenizedTweets = tokenizedTweets;
		}
		
		createInstances(tokenizedTweets);
	}
	
	public void createInstances(List<String[]> tokenizedTweets) {
		
		RunTimer timer = new RunTimer();
		
		// Step 2. Find the unigrams and convert them to an index map.
		timer.reset().start();
		List<String> unigramList = UnigramModel.buildUnigramVocabulary(tokenizedTweets);
		Map<String,Integer> wordToIndexMap = buildWordToIndexMap(unigramList);
		timer.stop();
		final int unigramCount = unigramList.size();
		LOG.debug("Found {} unigrams.  {}", unigramCount, timer.getFormattedRunTime());

		// Step 3. Translate tweets to bigrams.
		timer.reset().start();
		List<BiGram[]> bigramTweets = new ArrayList<BiGram[]>(tokenizedTweets.size());
		for( int c = 0; c < tokenizedTweets.size(); c++ ) {
			String[] currentTweet = tokenizedTweets.get(c);
			BiGram[] currentBigrams = new BiGram[currentTweet.length-1];
			
			for(int i = 0; i < currentTweet.length - 1; i++) {
				int token1Index = wordToIndexMap.get(currentTweet[i]).intValue();
				int token2Index = wordToIndexMap.get(currentTweet[i+1]).intValue();
				currentBigrams[i] = new BiGram(unigramCount, token1Index, token2Index);
			}
			
			bigramTweets.add(currentBigrams);
		}
		timer.stop();
		LOG.debug("Translated tweets to bigrams. {}", timer.getFormattedRunTime());
		tokenizedTweets = null; // don't need this anymore
		
		// Step 4. Create the bigram vocabulary.
		timer.reset().start();
		List<BiGram> bigramList = buildBigramVocabulary(bigramTweets);
		Map<BiGram,Integer> bigramToIndexMap = buildBigramToIndexMap(bigramList);
		timer.stop();
		final int bigramCount = bigramList.size();
		LOG.debug("Found {} bigrams. {}", bigramCount, timer.getFormattedRunTime());
				
		// Step 5. Create the attributes and the instances dataset.
		timer.reset().start();
		instances = createInstancesDataset(unigramList, bigramList);
		timer.stop();
		LOG.debug("Created instances. {}" , timer.getFormattedRunTime());
		unigramList = null; // don't need this anymore
		bigramList = null;
		
		setArffStructure(instances);
		
		// Step 6. Create the feature vector for each tweet and save to instances dataset.
		LOG.debug("Populating the instances dataset.");
		
		timer.reset().start();
		final int numTweets = bigramTweets.size();
		for (int c = 0; c < bigramTweets.size(); c++ ) {
			BiGram[] currentTweet = bigramTweets.get(c);
			
			if( c % 1000 == 0 )
				LOG.debug("Current tweet:\t{}/{}  {}", c, numTweets, timer.getFormattedRunTime());
			
			double[] values = new double[bigramCount+1];
			Instance currentDataPoint = new Instance(1, values); // +1 because of the class label
			currentDataPoint.setDataset(instances);
			
			for(int i = 0; i < currentTweet.length - 1; i++) {
				int featureIndex = bigramToIndexMap.get(currentTweet[i]);
				
				int value = (int)currentDataPoint.value(featureIndex);
				value++;
				currentDataPoint.setValue(featureIndex, (double)value);
			}
			
			
			AnnotatedTweet aTweet = dataFile.get(c);
			
			String[] classes = { "negative", "neutral", "positive", "mixed" }; // the class labels ( we need this to make naive bayes work )
			
			currentDataPoint.setValue(bigramCount, classes[aTweet.annotationFinal+1]);  // the last feature is the class label
			
			writeInstance(currentDataPoint);
		}
		
		closeWriter();
		timer.stop();
		LOG.debug("Computed bigrams in {}.", timer.getFormattedRunTime());
	}

	private Instances createInstancesDataset(List<String> unigramList, List<BiGram> bigramList) {
		
		final int bigramCount = bigramList.size();
		
		LOG.debug("FastVector size = {} + 1", bigramCount);
		FastVector attrInfo = new FastVector(bigramCount + 1); // +1 for the class label
		
		// all the bigrams
		for( int i = 0; i < bigramCount; i++ ) {
			final BiGram currentBigram = bigramList.get(i);
			final String featureName = unigramList.get(currentBigram.token1) + "--" + unigramList.get(currentBigram.token2);
			//LOG.info("Feature name: {}", featureName);
			Attribute currentAttribute = new Attribute(featureName);
			attrInfo.addElement(currentAttribute);				
		}
		
		FastVector nominalClassValues = new FastVector();
		nominalClassValues.addElement("negative");
		nominalClassValues.addElement("neutral");
		nominalClassValues.addElement("positive");
		nominalClassValues.addElement("mixed");
		
				
		Attribute finalClassLabel = new Attribute("Final Class Label", nominalClassValues);
		attrInfo.addElement(finalClassLabel);
		
		LOG.debug("Attrinfo size {}", attrInfo.size());
		
		// name of the training set:
		String trainingSetName = "bigram";
		
		return new Instances(trainingSetName, attrInfo, dataFile.size());
	}
	
	/**
	 * Create a vocabulary of all the unique bigrams.
	 */
	private List<BiGram> buildBigramVocabulary(List<BiGram[]> bigramTweets) {
		Set<BiGram> vocab = new TreeSet<BiGram>();
		for(BiGram[] bigramTweet : bigramTweets) {
			for(BiGram currentBigram : bigramTweet) {
				vocab.add(currentBigram);
			}
		}

		return new ArrayList<BiGram>(vocab);
	}
	
	/**
	 * @returns A map from each word to its index in the list.
	 */
	private Map<String, Integer> buildWordToIndexMap(List<String> unigramList) {
		Map<String, Integer> map = new HashMap<String, Integer>();
		
		for( int i = 0; i < unigramList.size(); i++) {
			map.put(unigramList.get(i), new Integer(i));
		}
		
		return map;
	}
	
	/**
	 * @returns A map from each bigram to its index in the list.
	 */
	private Map<BiGram, Integer> buildBigramToIndexMap(List<BiGram> bigramList) {
		Map<BiGram, Integer> map = new TreeMap<BiGram, Integer>();
		
		for( int i = 0; i < bigramList.size(); i++) {
			map.put(bigramList.get(i), new Integer(i));
		}
		
		return map;
	}
	
	private class BiGram implements Comparable<BiGram> {
		public final int token1;
		public final int token2;
		public final int unigramCount;
		public BiGram(int unigramCount, int token1, int token2) {
			this.token1 = token1;
			this.token2 = token2;
			this.unigramCount = unigramCount;
		}
		@Override
		public int hashCode() {
			//LOG.debug("Hashing {} {}", token1, token2);
			return (token1 * unigramCount) + token2;
		}
	//	@Override
		public int compareTo(BiGram o) {
			if( token1 == o.token1) {
				if( token2 == o.token2 ) {
					return 0;
				}
				else return token2 - o.token2;
			}
			else return token1 - o.token1;
		}
	}
	
	public static void main(String[] args) throws FileNotFoundException, IOException {
		
		createSet("Obama"   , "training");
		createSet("Romney"  , "training");
		createSet("Together", "training");
		
		createSet("Obama"   , "testing");
		createSet("Romney"  , "testing");
		createSet("Together", "testing");
	}
	
	/**
	 * @param process This string must be one of these (case sensitive): Obama, Romney, or Together
	 * @param set This string must be one of these: training, testing
	 */
	public static void createSet(String process, String set) {
		InputStream inputFile = ClassLoader.class.getResourceAsStream("/data/"+set+"-"+process+"-tweets.csv");
		
		BigramModel creator = new BigramModel(
				"/home/cosmin/Dropbox/Final Project/datasets/report/"+set+"-tokenized-tweets-"+process.toLowerCase()+".ser.bz2",
				"/home/cosmin/Dropbox/Final Project/datasets/report/"+set+"/"+set+"-"+process.toLowerCase()+"-bigram.arff.gz");
		
		creator.setInputSource(new InputStreamReader(inputFile));
		creator.createInstances();
	}
}
